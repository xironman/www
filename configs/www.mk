# ---------------------------------------------------------------
# Build imwww
# ---------------------------------------------------------------

root-verify:
	@if [ $(UID) -ne 0 ]; then \
		$(MSG11) "You must be root." $(EMSG); \
		exit 1; \
	fi

opkg-verify:
	@if [ "$(OPKG_DIR)" = "" ] || [ ! -f $(OPKG_DIR)/opkg-build ]; then \
		$(MSG11) "Can't find opkg-build.  Try setting OPKG= to the directory it lives in." $(EMSG); \
		exit 1; \
	fi

# Retrieve package
.$(WWW_T)-get $(WWW_T)-get: 
	@$(MSG) "================================================================"
	@$(MSG2) "Retrieving files" $(EMSG)
	@$(MSG) "================================================================"
	@mkdir -p $(WWW_SRCDIR)
	@cp -r $(DIR_WWW) $(WWW_SRCDIR)
	@cp -r $(DIR_REST) $(WWW_SRCDIR)
	@rm -f $(WWW_SRCDIR)/imrest/version.txt
	@cp -r $(TOPDIR)/version.txt $(WWW_SRCDIR)/imrest/
	@touch .$(subst .,,$@)

.$(WWW_T)-get-patch $(WWW_T)-get-patch: .$(WWW_T)-get
	sed -i 's/\[-BANNER-\]/$(WWW_BANNER)/g' $(WWW_SRCDIR)/imwww/index.html
	@touch .$(subst .,,$@)

# Unpack packages
.$(WWW_T)-unpack $(WWW_T)-unpack: .$(WWW_T)-get-patch
	@touch .$(subst .,,$@)

# Apply patches
.$(WWW_T)-patch $(WWW_T)-patch: .$(WWW_T)-unpack
	@touch .$(subst .,,$@)

.$(WWW_T)-init $(WWW_T)-init: .$(WWW_T)-patch
	@touch .$(subst .,,$@)

.$(WWW_T)-config $(WWW_T)-config: 

# Build the package
$(WWW_T): .$(WWW_T)

.$(WWW_T): .$(WWW_T)-init 
	@make --no-print-directory $(WWW_T)-config
	@touch .$(subst .,,$@)

$(WWW_T)-files:
	@$(MSG) "================================================================"
	@$(MSG2) "WWW Files" $(EMSG)
	@$(MSG) "================================================================"
	@ls -l $(WWW_SRCDIR)

# Package it as an opkg 
opkg pkg $(WWW_T)-pkg: .$(WWW_T)
ifeq ($(PLATFORM),any)
	@make --no-print-directory root-verify opkg-verify
	@mkdir -p $(PKGDIR)/opkg/imwww/CONTROL
	@mkdir -p $(PKGDIR)/opkg/imwww/home/httpd/imwww
	@mkdir -p $(PKGDIR)/opkg/imwww/home/httpd/imrest
	@mkdir -p $(PKGDIR)/opkg/imwww/etc/init.d
	@mkdir -p $(PKGDIR)/opkg/imwww/usr/bin
	@mkdir -p $(PKGDIR)/opkg/imwww/var/spool/cron/crontabs
	@cp -ar $(WWW_SRCDIR)/imwww/* $(PKGDIR)/opkg/imwww/home/httpd/imwww
	@cp -ar $(WWW_SRCDIR)/imrest/* $(PKGDIR)/opkg/imwww/home/httpd/imrest
	@cp -ar $(SRCDIR)/scripts/* $(PKGDIR)/opkg/imwww/etc/init.d
	@cp -ar $(TOPDIR)/scripts/runserver.sh $(PKGDIR)/opkg/imwww/usr/bin
	@cp -ar $(TOPDIR)/src/scripts/root $(PKGDIR)/opkg/imwww/var/spool/cron/crontabs
	@rm -f $(PKGDIR)/opkg/imwww/home/httpd/monkey/images/*.xcf
	@cp $(SRCDIR)/opkg/control $(PKGDIR)/opkg/imwww/CONTROL/control
	@cp $(SRCDIR)/opkg/postinst $(PKGDIR)/opkg/imwww/CONTROL/preinst
	@cp $(SRCDIR)/opkg/postinst $(PKGDIR)/opkg/imwww/CONTROL/postinst
	@cp $(SRCDIR)/opkg/prerm $(PKGDIR)/opkg/imwww/CONTROL/prerm
	@cp $(SRCDIR)/opkg/debian-binary $(PKGDIR)/opkg/imwww/CONTROL/debian-binary
	@sed -i 's%\[VERSION\]%'`cat $(TOPDIR)/version.txt`'%' $(PKGDIR)/opkg/imwww/CONTROL/control
	@sed -i '/PIPLAYER/,+9d' $(PKGDIR)/opkg/imwww/CONTROL/postinst
	@sed -i 's%bash -p%ash%' $(PKGDIR)/opkg/imwww/usr/bin/runserver.sh
	@chmod +x $(PKGDIR)/opkg/imwww/CONTROL/preinst
	@chmod +x $(PKGDIR)/opkg/imwww/CONTROL/postinst
	@chmod +x $(PKGDIR)/opkg/imwww/CONTROL/prerm
	@chmod +x $(PKGDIR)/opkg/imwww/usr/bin/*
	@chown -R root.root $(PKGDIR)/opkg/imwww/
	@cd $(PKGDIR)/opkg/ && $(OPKG_DIR)/opkg-build -O imwww
	@cp $(PKGDIR)/opkg/*.opk $(PKGDIR)/
	@rm -rf $(PKGDIR)/opkg
endif
ifeq ($(PLATFORM),piplayer)
	@make --no-print-directory root-verify opkg-verify
	@mkdir -p $(PKGDIR)/opkg/imwww/CONTROL
	@mkdir -p $(PKGDIR)/opkg/imwww/home/httpd/imwww
	@mkdir -p $(PKGDIR)/opkg/imwww/etc/init.d
	@mkdir -p $(PKGDIR)/opkg/imwww/usr/bin
	@cp -ar $(WWW_SRCDIR)/imwww/* $(PKGDIR)/opkg/imwww/home/httpd/imwww
	@cp -ar $(TOPDIR)/scripts/runserver.sh $(PKGDIR)/opkg/imwww/usr/bin
	@rm -f $(PKGDIR)/opkg/imwww/home/httpd/monkey/images/*.xcf
	@cp $(SRCDIR)/opkg/control $(PKGDIR)/opkg/imwww/CONTROL/control
	@cp $(SRCDIR)/opkg/postinst $(PKGDIR)/opkg/imwww/CONTROL/preinst
	@cp $(SRCDIR)/opkg/postinst $(PKGDIR)/opkg/imwww/CONTROL/postinst
	@sed -i '/Create IoT/,+4d' $(PKGDIR)/opkg/imwww/CONTROL/postinst
	@cp $(SRCDIR)/opkg/prerm $(PKGDIR)/opkg/imwww/CONTROL/prerm
	@cp $(SRCDIR)/opkg/debian-binary $(PKGDIR)/opkg/imwww/CONTROL/debian-binary
	@sed -i 's%\[VERSION\]%'`cat $(TOPDIR)/version.txt`'%' $(PKGDIR)/opkg/imwww/CONTROL/control
	@sed -i 's%bash -p%ash%' $(PKGDIR)/opkg/imwww/usr/bin/runserver.sh
	@chmod +x $(PKGDIR)/opkg/imwww/CONTROL/preinst
	@chmod +x $(PKGDIR)/opkg/imwww/CONTROL/postinst
	@chmod +x $(PKGDIR)/opkg/imwww/CONTROL/prerm
	@chmod +x $(PKGDIR)/opkg/imwww/usr/bin/*
	@chown -R root.root $(PKGDIR)/opkg/imwww/
	@cd $(PKGDIR)/opkg/ && $(OPKG_DIR)/opkg-build -O imwww
	@cp $(PKGDIR)/opkg/*.opk $(PKGDIR)/
	@rm -rf $(PKGDIR)/opkg
endif

# Clean the packaging
pkg-clean $(WWW_T)-pkg-clean:
	@if [ "$(PKGDIR)" != "" ] && [ -d "$(PKGDIR)" ]; then rm -rf $(PKGDIR); fi

# Clean out a cross compiler build but not the CT-NG package build.
$(WWW_T)-clean: $(WWW_T)-pkg-clean
	@if [ "$(WWW_SRCDIR)" != "" ] && [ -d "$(WWW_SRCDIR)" ]; then rm -rf $(WWW_SRCDIR); fi
	@rm -f .$(WWW_T) .$(WWW_T)-get

# Clean out everything associated with WWW
$(WWW_T)-clobber: 
	@rm -rf $(BLDDIR) 
	@rm -f .$(WWW_T)-config .$(WWW_T)-init .$(WWW_T)-patch \
		.$(WWW_T)-unpack .$(WWW_T)-get .$(WWW_T)-get-patch
	@rm -f .$(WWW_T) 

