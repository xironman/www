<?php

/*
 * ---------------------------------------------------------------
 * IronMan routing.
 * This file contains the tests required to determine what
 * functions should be called for the UI.  
 *
 * It does not contain support for any of the REST API.
 * See devices.php for the REST API.
 * ---------------------------------------------------------------
 */

/* Initialization */
include "init.php";
$iminit = IMInit::Instance();
$iminit->setEnv();

ini_set('display_errors', 'On');
ini_set('error_log', $iminit->get("logpath") . '/imwww.err');
error_reporting(E_NOTICE);

error_log("Web Server path: " . $iminit->get("wspath"));
error_log("Log path: " . $iminit->get("logpath"));

/*
 * Debugging and other globally useful classes and functions.
 * Uncomment setLevel() to enable debugging.
 */
include "util.php";
$dbg = new PiboxLog();
$dbg->setLevel('DEBUG');

// Functions to sending network configs to piboxd.
include "netsettings.php";

// Functions to sending network configs to piboxd.
include "settings-write.php";

// Pair Mode handling.
include "pairmode.php";

/*
 * ---------------------------------------------------------------
 * Main routine - test function API
 * ---------------------------------------------------------------
 */
function main()
{
    global $dbg;
    global $iminit;

    $dbg->info("Web Server path: " . $iminit->get("wspath"));
    $dbg->info("Log path: " . $iminit->get("logpath"));
    $dbg->info("imstampPath: " . $iminit->get("imstampPath"));
    $dbg->info("debugstamp: " . $iminit->get("debugstamp"));
    $dbg->info("debugLogPath: " . $iminit->get("debugLogPath"));
    $dbg->info("debugStampPath: " . $iminit->get("debugStampPath"));
    $dbg->info("debugAPPath: " . $iminit->get("debugAPPath"));

    // This works because "function" is always set on the URL, even in POSTs.
    $function=$_GET["function"];
    $dbg->info("QUERY String: " . $_SERVER['QUERY_STRING'] );

    $iminit = IMInit::Instance();
    $pairing = $iminit->get("pairEnabled");
    $dbg->info("Pair enabled: " . $pairing);

    if ( $pairing == 1 )
    {
        // We can get here if we're saving the settings.
        if ( $function == "netsettings" )
        {
            $dbg->info("Calling netsettings.");
            netsettings();
        }
        else
        {
            $dbg->info("Entering Pair Mode.");
            pairMode();
        }
        return;
    }
    $dbg->info("Pairing not enabled. Ignoring request.");
    header("Location: /index.html");
    die();
}

main();

?>
