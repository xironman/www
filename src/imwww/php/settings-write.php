<?php

/*
 * ---------------------------------------------------------------
 * Save current network mode (wireless client or access point)
 * ---------------------------------------------------------------
 */
function setMode()
{
    global $dbg;

    if ( empty($_POST) )
    {
        $dbg->info("setMode: no POST data");
        return;
    }
    $dbg->info("setMode POST data: " . print_r($_POST, true));

    // Find the interface we're updating.
    if ( isset($_POST['mode']) ) 
    { 
        $mode = $_POST['mode']; 
    }
    else
    {
        $dbg->info("setMode: no MODE specified - ignoring set request.");
        print "Save failed: missing mode.";
        return;
    }

    $socket = getSocket(1);
    $header = 0x00000a03;   // MT_NET, MA_SETNETTYPE
    $size = strlen($mode);
    socket_write($socket, pack("I", $header), 4);
    socket_write($socket, pack("I", $size), 4);
    socket_write($socket, $mode, strlen($mode));
    usleep(20);

    // Close socket
    socket_close($socket);
}

/*
 * ---------------------------------------------------------------
 * Save data from the UI
 * ---------------------------------------------------------------
 */
function save()
{
    global $dbg;
    $dbg->info("save: Entered");

    if ( empty($_POST) )
    {
        $dbg->info("save: no POST data");
        return;
    }
    $dbg->info("save POST data: " . print_r($_POST, true));

    // The "type" determines what we're saving.
    if ( !isset($_POST['type']) ) 
    { 
        $dbg->info("save: missing TYPE in POST data");
        return;
    }
    $type = strtolower( $_POST['type'] );

    // Store the descriptor
    writeDescriptor();

    // Handle all other configurations.
    if ( saveWireless() == 1 )
    {
        $dbg->info("save: failed saveWireless");
        print "Configuration failed.";
        return;
    }
    $noap = $_POST['noap'];
    if ( $noap == 0 )
    {
        if ( saveAccessPoint() == 1 )
        {
            $dbg->info("save: failed saveAccessPoint");
            print "Configuration failed.";
            return;
        }
    }

    /* This sets the wlan interface to use dhcp. */
    if ( saveIPV4() == 1 )
    {
        $dbg->info("save: failed saveIPV4");
        print "Configuration failed.";
        return;
    }
    print "Configuration saved.";
}

// Save the monitor descriptor.  This is something like 
// "bedroom", "living room", "basement", etc.
function writeDescriptor()
{
    $descriptor = $_POST['descriptor']; 
    $monitor_dir = "/etc/monitor";
    if ( !file_exists( $monitor_dir ) )
    {
        mkdir($monitor_dir, 0644);
    }
    $descriptor_file = $monitor_dir . "/descriptor";
    unlink($descriptor_file);
    if ( $handle = fopen($descriptor_file, 'w') )
    {
        fwrite($handle, $descriptor);
    }
    else
    {
        fwrite($handle, "No descriptor available.");
    }
}

// Save an interface configuration.
function saveIPV4()
{
    global $dbg;
    $dbg->info("saveIPV4: Entered");

    /*
     * Build the payload for the message.
     * If a field is not used or unavailable it is set to -1.
     * Order of fields for the payload:
     * 1. Interface (name of interface)
     * 2. Enabled state (yes/no)
     * 3. Configuration type (DHCP/Static)
     * 4. IP Address 
     * 5. Netmask
     * 6. Gateway   - Not required for Ironman
     * 7. DNS 0     - Not required for Ironman
     * 8. DNS 1     - Not required for Ironman
     * 9. DNS 2     - Not required for Ironman
     *
     * For Ironman and piplayer, it's rather static in structure.
     *   wlan0: wlan0:yes:DHCP:::::::
     *   uap0: uap0:yes:Static:APIP:255.255.255.0::::
     */

    // Configure the wifi on the local network
    $dbg->info("Sending Wifi config to piboxd");
    $msg1 = "wlan0:yes:DHCP::::::";

    $socket = getSocket(1);
    $header = 0x00000703;   // MA_SETIPV4, MT_NET
    $size = strlen($msg1);
    socket_write($socket, pack("I", $header), 4);
    socket_write($socket, pack("I", $size), 4);
    socket_write($socket, $msg1, strlen($msg1));
    socket_close($socket);

    $dbg->info("saveIPV4: done.");
    return 0;
}

// Save wireless client configuration.
function saveWireless()
{
    global $dbg;
    $dbg->info("saveWireless: Entered");

    /* This comes from Pair Mode boot configuration */
    $ssid = $_POST['wcssid'];
    $security = $_POST['wcsecurity'];
    $pw = $_POST['wcpw'];
    $msg = $ssid . ":" . $security . ":" . $pw;

    $dbg->info("Sending MT_NET, MA_SETWIRELESS message to piboxd: msg = " . $msg );
    $socket = getSocket(1);
    if ($socket === false)
    {
        $dbg->error("Can't get socket to MT_NET/MA_SETWIRELESS");
        print "Failed to connect to monitor.\n";
        return 1;
    }
    $header = 0x00000903;   // MT_NET, MA_SETWIRELESS
    $size = strlen($msg);
    socket_write($socket, pack("I", $header), 4);
    socket_write($socket, pack("I", $size), 4);
    socket_write($socket, $msg, strlen($msg));
    socket_close($socket);

    $dbg->info("saveWireless: done.");
    return 0;
}

// Save access point configuration
function saveAccessPoint()
{
    global $dbg;
    $dbg->info("saveAccessPoint: Entered");

    /* Make sure dhcpd.conf template exists */
    $tmpl = "/etc/network/dhcpd.conf.uap.tmpl";
    if ( !file_exists($tmpl) )
    {
        $dbg->error("Missing uap dhcpd template.");
        print "Failed to configure sensor network: Missing template.\n";
        return 1;
    }

    /* 
     * This comes from Pair Mode boot configuration 
     * Note that the base address is hard coded for IoT 
     * devices to make life easier for the consumer.
     */
    $ssid = $_POST['apssid'];
    $channel = $_POST['apchannel'];
    $pw = $_POST['appw'];
    $base = $_POST['apip'];
    $interface = "uap0";
    $msg = $ssid . ":" . $channel . ":" . $pw . ":" . $base . ":" . $interface;

    // Save access point configuration information
    $dbg->info("Sending MT_NET/MA_SETAP message to piboxd: msg = ". $msg);
    $socket = getSocket(1);
    if ($socket === false)
    {
        $dbg->error("Can't get socket to MT_NET/MA_SETAP");
        print "Failed to connect to monitor.\n";
        return 1;
    }
    $header = 0x00000803;   // MT_NET, MA_SETAP
    $size = strlen($msg);
    socket_write($socket, pack("I", $header), 4);
    socket_write($socket, pack("I", $size), 4);
    socket_write($socket, $msg, strlen($msg));
    socket_close($socket);

    /* Read uap0 dhcpd template */
    $dbg->info("Reading template " . $tmpl);
    $page = file_get_contents($tmpl);

    /* Strip last octet from base - we assume network address is 24/8 */
    $fields = explode(".", $base);
    $base = "$fields[0].$fields[1].$fields[2]";

    /* Update template with base address */
    $page = str_replace("[BASENET]", $base, $page);
    $dbg->info("Updated dhcpd configuration\n" . $page);

    /* Write updated config to it's real home: /etc/network/dhcpd.conf.uap */
    $filename = "/etc/network/dhcpd.conf.uap";
    file_put_contents($filename, $page, LOCK_EX);

    /* And we're done */
    $dbg->info("saveAccessPoint: done.");
    return 0;
}

?>
