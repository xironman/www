<?php

/*
 * ---------------------------------------------------------------
 * In PAIR mode we present only the configuration page for both
 * the AP network and the monitor's Internet connection via wifi.
 *
 * Once the user accepts the configuration it is saved and the
 * system is rebooted.
 * ---------------------------------------------------------------
 */
function pairMode()
{
    global $dbg;

    $iminit = IMInit::Instance();
    $noap = $iminit->get("noap");
    $dbg->info("AP enabled: " . $noap);

    // The template for the network configuration
    if ( $noap == 1 )
    {
        $tmpl = "netconfig-noap.tmpl";
    }
    else
    {
        $tmpl = "netconfig.tmpl";
    }
    $dbg->info("Reading template " . $tmpl);
    $page = file_get_contents($tmpl);

    /* 
     * --------------------------------------------------
     * Configure Internet Access for the monitor.
     * --------------------------------------------------
     */
    $html = "<center><h1>Internet Connection</h1></center>\n";

    // Start a table of elements.
    $html .= "<table>\n";

    // Descriptor
    $html .= "<tr>\n";
    $html .= "    <td> Location </td>\n";
    $html .= "    <td> ";
    $html .= "<input type=\"text\" id=\"DESCRIPTOR\" >";
    $html .= "    </td>\n";
    $html .= "</tr>\n";

    // SSID
    $html .= "<tr>\n";
    $html .= "    <td> SSID </td>\n";
    $html .= "    <td> ";
    $html .= "<input type=\"text\" id=\"WCSSID\" >";
    $html .= "    </td>\n";
    $html .= "</tr>\n";

    // Security type
    $securityType = array ( "WPA & WPA2 Personal", "WPA & WPA2 Enterprise" );

    $html .= "<tr>\n";
    $html .= "    <td> Security </td>\n";
    $html .= "    <td>";
    $html .= "<select id=\"WCSEC\" >\n";
    foreach($securityType as $sec)
    {
        $selected = "";
        if ( $sec == "1" ) { $selected = "selected"; }
        $html .= "<option value=\"" . $sec . "\" " . $selected . ">" . $sec . "</option>\n";
    }
    $html .= "</select>\n";
    $html .= "    </td>\n";
    $html .= "</tr>\n";

    // Password
    $html .= "<tr>\n";
    $html .= "    <td> Password </td>\n";
    $html .= "    <td>";
    $html .= "<input type=\"password\" id=\"WCPW\" onchange=\"wcpwChanged('v')\"/>";
    $html .= "<input type=\"text\" id=\"WCPWhidden\"" . 
                " style='display:none;' onchange=\"wcpwChanged('h')\"/>";
    $html .= "    </td>\n";
    $html .= "</tr>\n";

    // Show/Hide password button
    $html .= "<tr>\n";
    $html .= "    <td colspan=\"2\" > ";
    $html .= "    <label class=\"ShowHide\" > ";
    $html .= "<input type=\"checkbox\" id=\"WCPWshow\" onclick=\"wcpwShow()\"/>Show Password";
    $html .= "    </label> ";
    $html .= "    </td>\n";
    $html .= "</tr>\n";

    // Pass thru the AP enable state.
    $html .= "<tr>\n";
    $html .= "<tr><input id=\"NOAP\" name=\"NOAP\" type=\"hidden\" value=\"" . $noap . "\">\n";
    $html .= "</tr>\n";

    $html .= "</table>\n";

    if ( $noap == 0 )
    {
        /*
         * --------------------------------------------------
         * Configure the Sensor Network Access Point
         * --------------------------------------------------
         */
        $html .= "<center><h1>Sensor Network</h1></center>\n";

        // Start a table of elements.
        $html .= "<table>\n";

        /*
         * Generate the HTML for the AP configuration.
         * This includes:
         * 1. SSID
         * 2. Channel number
         * 3. Password
         */

        // SSID
        $html .= "<tr>\n";
        $html .= "    <td> SSID </td>\n";
        $html .= "    <td> ";
        $html .= "<input type=\"text\" id=\"APSSID\" >";
        $html .= "    </td>\n";
        $html .= "</tr>\n";

        // Channel type
        $channels = array ( "1", "5", "9", "13" );

        $html .= "<tr>\n";
        $html .= "    <td> Channel </td>\n";
        $html .= "    <td>";
        $html .= "<select id=\"APCH\" >\n";
        foreach($channels as $ch)
        {
            $selected = "";
            if ( $ch == "1" ) { $selected = "selected"; }
            $html .= "<option value=\"" . $ch . "\" " . $selected . ">" . $ch . "</option>\n";
        }
        $html .= "</select>\n";
        $html .= "    </td>\n";
        $html .= "</tr>\n";

        // Password
        $html .= "<tr>\n";
        $html .= "    <td> Password </td>\n";
        $html .= "    <td>";
        $html .= "<input type=\"password\" id=\"APPW\"" . " onchange=\"appwChanged('v')\"/>";
        $html .= "<input type=\"text\" id=\"APPWhidden\" style='display:none;' ".
                    "onchange=\"appwChanged('h')\"/>";
        $html .= "    </td>\n";
        $html .= "</tr>\n";

        // Show/Hide password button
        $html .= "<tr>\n";
        $html .= "    <td colspan=\"2\" > ";
        $html .= "    <label class=\"ShowHide\" > ";
        $html .= "<input type=\"checkbox\" id=\"APPWshow\" onclick=\"appwShow()\"/>Show Password";
        $html .= "    </label> ";
        $html .= "    </td>\n";
        $html .= "</tr>\n";

        // Provide base network address
        $html .= "<tr>\n";
        $html .= "    <td> Static IP </td>\n";
        $html .= "    <td> ";
        $html .= "<input type=\"text\" id=\"APIP\" >";
        $html .= "    </td>\n";
        $html .= "</tr>\n";

        // Close the table element
        $html .= "</table>\n";
    }

    // Update the page with the generated HTML
    $page = str_replace("[NETCONFIG]", $html, $page);
    print $page;
}

?>
