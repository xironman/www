<?php
/*
 * ---------------------------------------------------------------
 * Singleton used to establish global variables and environment
 * required for the rest of the web services.
 *
 * This is instantiated by ironman.php first, then that instance
 * can be used to retrieve global variables.
 *
 * Note: this does not use $GLOBALS.
 * ---------------------------------------------------------------
 */

/* 
 * Global Constants
 */
define("PAIR_BUTTON", 4);

/*
 * ---------------------------------------------------------------
 * Initialization class
 * ---------------------------------------------------------------
 */
final class IMInit
{
    /*
     * Private variables
     */
    static private $docr;
    static private $dd;
    static private $noap = 0;

    /*
     * The debug stamp file should be 
     *   $TOPDIR/src/imwww/data/debug
     * in order to run in debug mode.
     * This is only useful for developers.
     */
    static private $debugStamp;

    /* Debug paths are relative to the document root */
    static private $debugLogPath;
    static private $debugWSPath;
    static private $debugStampPath;
    static private $debugAPPath;

    /* Run time paths */
    static private $runtimeLogPath  = "/var/log";
    static private $runtimeWSPath   = "/etc/monkey";
    static private $imstampPath     = "/etc/gpio/pair";
    static private $noapPath        = "/etc/noap";

    /* Initialize the paths array. */
    static private $paths;

    /*
     * -------------------------------------------------
     * Force single instance of the class.
     * -------------------------------------------------
     */

    /*
     * Call this method to get singleton
     * and initialize the paths array.
     * @return UserFactory
     */
    public static function Instance()
    {
        static $inst = null;
        if ($inst === null) 
        {
            $inst = new IMINit();
            self::$docr            = $_SERVER['DOCUMENT_ROOT'];
            self::$dd              = self::$docr."/data";
            self::$debugStamp      = self::$dd."/debug";
            self::$debugLogPath    = self::$dd."/log";
            self::$debugWSPath     = self::$dd."/monkey";
            self::$debugStampPath  = self::$dd."/pair";
            self::$debugAPPath     = self::$dd."/noap";
            self::$paths = array(
                'wspath'      => self::$runtimeWSPath,
                'logpath'     => self::$runtimeLogPath,
                'imstampPath' => self::$imstampPath,
                'noappath'    => self::$noapPath,
                'debugstamp'  => self::$debugStamp,
                'debugLogPath'=> self::$debugLogPath,
                'debugStampPath'=> self::$debugStampPath,
                'debugAPPath'=> self::$debugAPPath,
                'noap'        => 0,
                'pairEnabled' => 0);
            // $GLOBALS['wspath'] = self::$runtimeWSPath;
            // $GLOBALS['logpath'] = self::$runtimeLogPath;
        }
        return $inst;
    }

    /*
     * Private constructor so nobody else can instance it
     */
    private function __construct()
    {
    }

    /*
     * -------------------------------------------------
     * Private functions
     * -------------------------------------------------
     */

    /*
     * Check boot mode.
     */
    private function getBootMode()
    {
        if ( !file_exists(self::$paths['imstampPath']) )
        {
            return(0);
        }
        return(1);
    }

    /*
     * Check AP use mode.
     */
    private function getAPMode()
    {
        if ( file_exists(self::$paths['noappath']) )
        {
            return(1);
        }
        return(0);
    }

    /*
     * Remove a directory path and all its contents.
     */
    private function rrmdir($dir)
    {
        if (is_dir($dir))
        {
            $objects = scandir($dir); 
            foreach ($objects as $object)
            {
                if ($object != "." && $object != "..")
                {
                    if (is_dir($dir."/".$object))
                        $this->rrmdir($dir."/".$object);
                    else
                        unlink($dir."/".$object); 
                } 
            }
            rmdir($dir); 
        } 
    }

    /*
     * -------------------------------------------------
     * Public functions
     * -------------------------------------------------
     */

    /* 
     * Set up our operational environment.
     */
    public function setEnv()
    { 
        static $once = 0;

        /* Don't allow setup more than once. */
        if ( $once == 1 )
            return;

        /* 
         * If the debug stamp is found then we're running in debug mode
         * directly out of the source tree (not in production).
         */
        if ( file_exists(self::$debugStamp) )
        {
            self::$paths['wspath']      = self::$debugWSPath;
            self::$paths['logpath']     = self::$debugLogPath;
            self::$paths['imstampPath'] = self::$debugStampPath;
            self::$paths['noappath']    = self::$debugAPPath;

            if ( ! file_exists(self::$debugWSPath) )
            {
                mkdir(self::$debugWSPath, 0700, true);
            }
            if ( ! file_exists(self::$debugLogPath) )
            {
                mkdir(self::$debugLogPath, 0700, true);
            }
        }
        else
        {
            /* 
             * Make sure the data directory doesn't exist 
             * if we're running in production mode, unless we're using NOAP.
             */
            if ( ! self::getAPMode() )
                $this->rrmdir(self::$dd);
        }

        /* Determine if the PAIR button is pressed. */
        self::$paths['pairEnabled']  = self::getBootMode();

        /* Determine if the AP settings should be used. */
        self::$paths['noap']  = self::getAPMode();

        $once = 1;
    }

    /*
     * Retrieve a path setting.
     */
    public function get($pathname)
    { 
        return IMInit::$paths["$pathname"];
    }

    /*
     * Check if the AP settings should be used.
     */
    public function getAP()
    {
        return self::$paths['noap'];
    }
}

?>
