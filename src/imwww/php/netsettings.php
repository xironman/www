<?php

/*
 * ---------------------------------------------------------------
 * Handle configuration requests of the piboxd daemon
 * and the web interface.
 * ---------------------------------------------------------------
 */
function netsettings()
{
    global $dbg;

    $dbg->info("Entered netsettings");

    // Subpages are handled first.  GETs return HTML snippets.
    // POSTs update the server, and may return HTML snippets.
    if ( isset($_POST['subPage']) )
    {
        $subPage = $_POST['subPage'];
        $dbg->info("Subpage is set: " . $subPage);

        if ( strcmp($subPage, "save") == 0 ) 
        {
            $dbg->info("Calling save()");
            save(); 
        }
        else if ( strcmp($subPage, "reboot") == 0 )
        {
            reboot();
        }
        return;
    }

    // Any other option is currently invalid.
    header( 'Location: /index.html' ) ;
    return;
}
    
/*
 * ---------------------------------------------------------------
 * Send a message to piboxd to reboot.
 * ---------------------------------------------------------------
 */
function reboot()
{
    global $dbg;

    $dbg->info("entered reboot()");
    $mode=" ";

    $socket = getSocket(1);
    $header = 0x00000106;   // MA_REBOOT, MT_SYS
    $size = strlen($mode);
    socket_write($socket, pack("I", $header), 4);
    socket_write($socket, pack("I", $size), 4);
    socket_write($socket, $mode, strlen($mode));
    usleep(20);

    // Close socket
    socket_close($socket);
}

?>
