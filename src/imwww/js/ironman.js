<!--  Hide from old browsers.

    /* 
     * ==================================
     * IronMan:  Front Page Javascript
     * ==================================
     */

    /* 
     * ----------------------------------
     * Page Setup
     * ----------------------------------
     */
    function loadSetup() 
    {
        // Get the menu based on address of requesting browser.
        $.get( "/php/ironman.php?function=frontpage", 
                function(data) {
                    $( "#frontPage" ).html( data );
                }
        );

        // Get the dialog but don't show it.
        $( "#dialog" ).dialog({ autoOpen: false });
    }

-->
