<!--  Hide from old browsers.

    /*
     * ==================================
     * IronMan:  Network configuration Javascript
     * ==================================
     */

    /*
     * ----------------------------------
     * Global variables
     * ----------------------------------
     */

    /*
     * ----------------------------------
     * Utilities
     * ----------------------------------
     */

    // Go back to home page
    function goHome()
    {
        window.location = '/';
    }

    // Manage a dialog window
    function doDialog( dTitle, dText )
    {
        if ( dText != "" )
            $( "#dialog" ).html( dText );
        else
            $( "#dialog" ).html( "No response from server." );
        $( "#dialog" ).dialog( "option", "title", dTitle );
        $( "#dialog" ).dialog(
            {
                modal: true,
                closeText: "Ok",
                draggable: false,
                resizable: false,
                position: ['center', 'center'],
                show: 'blind',
                hide: 'blind',
                width: 400,
                dialogClass: 'ui-dialog'
            });
        $( "#dialog" ).dialog("open");
    }

    // Validate an IP address
    function checkIP(ipaddr)
    {
        validIP = true;
        fields = ipaddr.split(".");
        if( fields.length == 4 )
        {
            for(i=0; i<4; i++)
            {
                num = parseInt(fields[i]);
                if(num <0 || num > 255)
                {
                    validIP = false;
                    break;
                }
            }
        }
        else
            validIP = false;
        return validIP;
    }

    // Show or hide the wireless client page's password field.
    function wcpwShow()
    {
        var isChecked = $("#WCPWshow").prop('checked');
        if (isChecked) {
            $('#WCPWhidden').val( $("#WCPW").val() );
            $('#WCPW').hide();
            $('#WCPWhidden').show();
        }
        else {
            $('#WCPW').val( $("#WCPWhidden").val() );
            $('#WCPW').show();
            $('#WCPWhidden').hide();
        }
    }

    // Update pw fields when matched pair changes.
    function wcpwChanged(type)
    {
        if ( type == "v" ) { $('#WCPWhidden').val( $("#WCPW").val() ); }
        else               { $('#WCPW').val( $("#WCPWhidden").val() ); }
    }

    // Show or hide the access point client page's password field.
    function appwShow()
    {
        var isChecked = $("#APPWshow").prop('checked');
        if (isChecked) {
            $('#APPWhidden').val( $("#APPW").val() );
            $('#APPW').hide();
            $('#APPWhidden').show();
        }
        else {
            $('#APPW').val( $("#APPWhidden").val() );
            $('#APPW').show();
            $('#APPWhidden').hide();
        }
    }

    // Update pw fields when matched pair changes.
    function appwChanged(type)
    {
        if ( type == "v" ) { $('#APPWhidden').val( $("#APPW").val() ); }
        else               { $('#APPW').val( $("#APPWhidden").val() ); }
    }

    // Submit must save both the Access Point and
    // Internet connection settings.
    function netconfigSubmit( )
    {
        /*
         * The monitors descriptor.
         */
        descriptor = $( "#DESCRIPTOR" ).val();

        /*
         * Is the AP being used?
         */
        noap      = $( "#NOAP" ).val();

        /*
         * AP configuration retrieval and validation.
         */
        if ( noap == 0 )
        {
            apssid    = $( "#APSSID" ).val();
            apch      = $( "#APCH option:selected").text().trim();
            appw      = $( "#APPW" ).val();
            apip      = $( "#APIP" ).val();

            if ( appw.length<7 )
            {
                doDialog( "Error", "Sensor Network password must be 7 or more characters");
                return;
            }
        }

        /*
         * Internet connections settings retrieval and validation.
         */
        wcssid = $( "#WCSSID" ).val();
        wcsec  = $( "#WCSEC option:selected").text().trim();
        wcpw   = $( "#WCPW" ).val();
        if ( wcssid.length<4 )
        {
            doDialog( "Error", "Internet connction SSID must be 4 or more characters");
            return;
        }
        if ( wcpw.length<7 )
        {
            doDialog( "Error", "Password must be 7 or more characters");
            return;
        }

        /*
         * Submit all the data at once.
         */
        if ( noap == 0 )
        {
            $.post( "/php/ironman.php?function=netsettings",
                {
                    descriptor: descriptor,
                    subPage: 'save',
                    type: 'combined',
                    apssid: apssid,
                    apchannel: apch,
                    appw: appw,
                    apip: apip,
                    wcssid: wcssid,
                    wcsecurity: wcsec,
                    wcpw: wcpw,
                    noap: noap
                },
                function(data, status) {
                    doDialog( "Save", data);
                }
            );
        }
        else
        {
            $.post( "/php/ironman.php?function=netsettings",
                {
                    descriptor: descriptor,
                    subPage: 'save',
                    type: 'combined',
                    wcssid: wcssid,
                    wcsecurity: wcsec,
                    wcpw: wcpw,
                    noap: noap
                },
                function(data, status) {
                    doDialog( "Save", data);
                }
            );
        }
    }

    // Reboot the computer.  This is only called from Pair Mode.
    // We wait about 60 seconds, which hopefully is long enough for the monitor to reboot.
    function reboot( )
    {
        $.post( "/php/ironman.php?function=netsettings",
            {
                subPage: 'reboot',
            },
            function(data, status) {
                doDialog( "Rebooting", "System is rebooting.  You will be redirected in 60 seconds.");
                setTimeout(goHome, 60000);
            }
        );
    }

    // Reset clears all input fields.
    function reset() 
    {
        $("#DESCRIPTOR").val('');
        $("#WCSSID").val('');
        $("#WCPW").val('');
        $('#WCPWhidden').val('');
        $('#WCPW').hide();
        document.getElementById("WCSEC").selectedIndex = "0";
        document.getElementById("WCPWshow").checked = false;

        /*
         * Is the AP being used?
         */
        noap      = $( "#NOAP" ).val();

        if ( noap == 0 )
        {
            $( "#APSSID" ).val('');
            $( "#APPW" ).val('');
            $('#APPW').hide();
            $('#APPWhidden').val('');
            $( "#APIP" ).val('');
            document.getElementById("APCH").selectedIndex = "0";
            document.getElementById("APPWshow").checked = false;
        }
    }

-->
