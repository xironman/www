/*
 * Init
 * 
 * Initialize configurations items based on how we are run.
 */

/*
 * Import Classes
 */
var fs     = require('fs');
var log    = require('./log');
var uuidv4 = require('uuid/v4');

/*
 * Private variables
 */
var dataDir = "data";
var pairEnabledStamp = "/etc/imnetconfig";
var ironmanDir = "/etc/ironman";
var monitorDir = "/etc/monitor";
var stampDir = ironmanDir + "/jarvis/";
var iotDir = ironmanDir + "/iot/";
var descriptorFile = monitorDir + "/descriptor";
var uuid = uuidv4();
var debug_enabled = false;

/*
 * Private Methods
 */

/*
 * If we're in debug mode we fake a call to imgpio
 * by testing if a stamp file exists.  If it does
 * then we're enabled.  If not, we're not.
 */
function fake_imgpio() {
    if ( !fs.existsSync(dataDir + "/imgpio") )
    {
        return(1);
    }
    else
    {
        return(0);
    }
}

/*
 * Public Methods
 */

/*
 * Setup variables that other classes share but that need
 * run time configuration.
 */
exports.setup = function (debug) {
    log.info("init.setup has been called. debug = " + debug);
    debug_enabled = debug;
    if ( debug )
    {
        pairEnabledStamp = dataDir + "/imnetconfig";
        ironmanDir = dataDir + "/ironman";
        stampDir = ironmanDir + "/jarvis/";
        iotDir = ironmanDir + "/iot/";
        monitorDir = dataDir + "/monitor";
        descriptorFile = monitorDir + "/descriptor";
    }
    console.log("Working directory       : " + __dirname);
    console.log("Data directory directory: " + dataDir);
    console.log("Ironman directory       : " + ironmanDir);
    console.log("Monitor directory       : " + monitorDir);
    console.log("Descriptor file         : " + descriptorFile);
    console.log("Pair Enabled stamp      : " + pairEnabledStamp);
    console.log("Monitor UUID            : " + uuid);
}

/*
 * Setters
 */
exports.setDataDir = function (dir) {
    dataDir = dir;
}

/* Disable the PAIR LED. */
exports.disablePairLED = function () {
    if ( debug_enabled )
    {
        return(0);
    }

    if ( !fs.existsSync("/usr/bin/imgpio") )
    {
        log.error("Can't find /usr/bin/imgpio.");
        return(0);
    }
    const execSync = require('child_process').execSync;
    code = execSync('imgpio -p 27 -w 0');
}

/* Enable the PAIR LED. */
exports.enablePairLED = function () {
    if ( debug_enabled )
    {
        return(0);
    }

    if ( !fs.existsSync("/usr/bin/imgpio") )
    {
        log.error("Can't find /usr/bin/imgpio.");
        return(0);
    }
    const execSync = require('child_process').execSync;
    code = execSync('imgpio -p 27 -w 1');
}

/*
 * Getters
 */
exports.getPairEnabled = function () {
    /*
     * This allows us to fake gpio.
     */
    if ( debug_enabled )
    {
        return( fake_imgpio() );
    }

    /*
     * Make sure imgpio is available.  It must be in the expected location.
     */
    if ( !fs.existsSync("/usr/bin/imgpio") )
    {
        log.error("Can't find /usr/bin/imgpio.");
        return(0);
    }

    /*
     * Test gpio pin.  Need to do this everytime since we might be sync'ing with
     * Jarvis but not in pair mode to configure devices.
     */
    const execSync = require('child_process').execSync;
    code = execSync('imgpio -p 4 -r');
    log.trace("PAIR Enabled state (0: on; 1: off): " + code);

    /* The GPIO pin is reverse of what you'd expect! We fix that here. */
    var state = 0;
    if ( code == 1 )
    {
        state = 0;
    }
    else
    {
        state = 1;
    }
    return(state);
}
exports.isDebug = function () {
    return(debug_enabled);
}
exports.getDataDir = function () {
    return(datadir);
}
exports.getIronmanDir = function () {
    return(ironmanDir);
}
exports.getStampDir = function () {
    return(stampDir);
}
exports.getIotDir = function () {
    return(iotDir);
}
exports.getDescriptorFile = function () {
    return(descriptorFile);
}
exports.getUUID = function () {
    return(uuid);
}
