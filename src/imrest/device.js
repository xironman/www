/*
 * Device Handler
 * 
 * Device (IoT) interactions via the server.
 */

/*
 * Import Classes
 */
var fs     = require('fs');
var log    = require('./log');
var init   = require('./init');
var crypto = require('./crypto');
var utils  = require('./utils');

/*
 * Initialization
 */

/*
 * Private Methods
 */

/*
 * Write to file the update retrieved from a sensor.
 * This is the callback for the utils.http_post() call in getDevices().
 */
function saveSensorData(ipaddr, json)
{
    if ( json == null )
    {
        log.error("No response from " + ipaddr);
        return;
    }

    var stampDir = init.getIotDir();
    var uuid = utils.getUUID("iot", ipaddr);

    /* Clean up the inbound json */
    var json = utils.stringify(JSON.parse(json));

    /* Decode the response */
    var message = "";
    try {
        message = crypto.decrypt_json(uuid, JSON.parse(json));
    }
    catch(ex){
        log.info("Skipping response from unrecognized client: " + ex);
        return;
    }

    /* Save the response */
    var filename = stampDir + ipaddr;
    fs.writeFile(filename, message, function (err) {
        if (err) {
            log.error("Failed to write stamp file: " + filename);
            log.error("Reason: " + err);
            return;
        }
        log.info("Updated device @ " + filename + " with " + message);
    });
}

/*
 * Public Methods
 */

/*
 *  Update a device state.
 */
exports.update = function (req, res, next) {
    log.info("device.update has been called.");
    var ipaddr = utils.toIPv4(req.connection.remoteAddress)

    var json;
    var message = "";

    /* Don't decrypt if from the local machine. */
    if ( (ipaddr === "127.0.0.1") || utils.localIP(ipaddr) )
    {
        /* pisensors will send this as properly formatted JSON, but as a string. */
        message = req.rawBody;
        log.info("Request rawBody from localhost: " + message);
    }
    else
    {
        try {
            message = crypto.decrypt("jarvis", req);
            log.info("Request params from jarvis: " + message);
        }
        catch(ex){
            log.info("Request from unrecognized client: " + ex);
            res.send(401);
            return(next);
        }
    }

    /* Verify the request contains the "update" command. */
    var json = JSON.parse(message);
    if ( json.command !== "update" )
    {
        log.error("Unauthorized - request doesn't match update.");
        res.send(401);
        return(next);
    }

    /* Verify the request contains the "uuid" key. */
    if ( typeof json.uuid === "undefined")
    {
        log.error("Unauthorized - request doesn't include remote sensor id.");
        res.send(400);
        return(next);
    }

    /* Verify the request contains the "state" key. */
    if ( typeof json.state == "undefined")
    {
        log.error("Unauthorized - request doesn't include new state.");
        res.send(400);
        return(next);
    }

    /* Find IPv4 of specified device */
    var ipaddr = utils.getIPv4(json.uuid);
    if ( ipaddr === null )
    {
        log.error("Update request does not match any paired devices: " + json.uuid);
        res.send(400);
        return(next);
    }

    /* Build an update packet */
    var response = {};
    response.state = json.state;
    log.info("Response: " + JSON.stringify(response));
    var crypt_json;
    try {
        crypt_json = crypto.encrypt(json.uuid, JSON.stringify(response));
        log.info(JSON.stringify(crypt_json));
    }
    catch(err) {
        log.error("Failed to encrypt getdevices for " + ipaddr + "; " + err);
        res.send(500);
        return(next);
    }

    /* Send it to the device and update the local configuration file */
    var data = JSON.stringify(crypt_json);
    log.info("Contacting " + ipaddr );
    var json;
    try {
        utils.http_post(ipaddr, "/set", data, saveSensorData);
    }
    catch(err) {
        log.error("Failed to contact device at " + ipaddr);
        return;
    }

    /* This responds before we know it worked, but that's fine for now. */
    res.json(200);
    return(next);
}

/*
 * Get status of all devices.
 * This request must come from a local (loopback) query.
 * It causes the monitor to update the device states for all
 * paired devices by querying them all.
 */
exports.getDevices = function (req, res, next) {
    log.info("device.getDevices has been called.");
    var ipaddr = utils.toIPv4(req.connection.remoteAddress)

    /* Only allow these requests from the local machine. */
    if ( ipaddr !== "127.0.0.1" )
    {
        if ( ! utils.localIP(ipaddr) )
        {
            log.error("GetDevices request is not from local system.  Ignoring: " + ipaddr);
            res.send(401);
            return(next);
        }
    }
    log.info("GetDevices request from local system.");

    /* Iterate all paired IoT devices. */
    var stampDir = init.getIotDir();
    fs.readdir( stampDir, function( err, files ) {
        if( err ) {
            log.error( "Couldn't iterate devices directory: " + err );
            res.send(401);
            return(next);
        }

        var entries = new Array();
        files.forEach( function( ipaddr, index ) {

            /* Get the UUID for the device */
            var uuid = utils.getUUID("iot", ipaddr);
            if ( uuid == null )
            {
                log.error("Failed to find uuid for " + ipaddr);
                return;
            }

            /* Encrypt the request - this helps device verify we're the correct monitor. */
            var crypt_json;
            try {
                crypt_json = crypto.encrypt(uuid, "getdevices");
                log.info(JSON.stringify(crypt_json));
            }
            catch(err) {
                log.error("Failed to encrypt getdevices for " + ipaddr + "; " + err);
                return;
            }

            /* query the device for update information */
            var data = JSON.stringify(crypt_json);
            log.info("Contacting " + ipaddr );
            var json;
            try {
                utils.http_post(ipaddr, "/query", data, saveSensorData);
            }
            catch(err) {
                log.error("Failed to contact device at " + ipaddr);
                return;
            }
        });
    });

    res.send(200);
    return(next);
}

/*
 * Get list of devices.
 * This request comes from Jarvis or similar clients.
 */
exports.list = function (req, res, next) {
    log.info("device.list has been called.");

    var json;
    var message = "";
    try {
        message = crypto.decrypt("jarvis", req);
    }
    catch(ex){
        log.info("Request from unrecognized client: " + ex);
        res.send(401);
        return(next);
    }
    if ( message != "getDevices" )
    {
        log.error("Unauthorized - decoded request doesn't match getDevices.");
        res.send(401);
        return(next);
    }

    log.info("Received GET /devices request; message = " + message);

    /* Iterate over directory to pull in each device configuration. */
    var stampDir = init.getIotDir();
    var json = '{ "monitor":"' + init.getUUID() + '", "devices": [';
    fs.readdir( stampDir, function( err, files ) {
        if( err ) {
            log.error( "Couldn't iterate devices directory: " + err );
            res.send(401);
            return(next);
        }

        files.forEach( function( file, index ) {
            var filename = stampDir + file;
            log.info( "Adding device: " + filename );
            var data = fs.readFileSync(filename, 'utf8');
            log.info("Device JSON: " + data);
            var str = utils.stringify(JSON.parse(data));
            json += str + ',';
        });

        /* Strip last comma, if needed */
        if ( files.length > 0 )
        {
            json=json.slice(0, -1);
        }

        json += ']}';
        log.info ("Device list: " + json)

        /* Encrypt the response */
        var ipaddr = utils.toIPv4(req.connection.remoteAddress);
        var jarvis_uuid = utils.getUUID("jarvis", ipaddr);
        var crypt_json;
        try {
            crypt_json = crypto.encrypt(jarvis_uuid, json);
            log.info(crypt_json);
        }
        catch(err) {
            log.error(err);
            res.send(500, JSON.stringify(err));
            return(next);
        }
        log.info(JSON.stringify(crypt_json));

        res.json(200, crypt_json);
        return(next);
    });
}

/*
 * Handle a device ping. This is a device powering up that thinks it
 * has previously been registered.  If we can decode it's request then
 * it is registered and we return 200.  If we can't decode it, then
 * we return 401.
 */
exports.ping = function (req, res, next) {
    log.info("device.ping has been called.");

    var json;
    var message = "";
    try {
        message = crypto.decrypt("iot", req);
    }
    catch(ex){
        log.info("Request from unrecognized client: " + ex);
        res.send(401);
        return(next);
    }
    if ( message != "ping" )
    {
        log.error("Unauthorized - decoded request doesn't match ping.");
        res.send(401);
        return(next);
    }

    log.info("Received valid ping request.");
    res.send(200);
    return(next);
}

/*
 *  Delete a device, which just removes it's registration file.
 */
exports.delete = function (req, res, next) {
    log.info("device.delete has been called.");
    var ipaddr = utils.toIPv4(req.connection.remoteAddress)

    var json;
    var message = "";

    /* Don't decrypt if from the local machine. */
    if ( (ipaddr === "127.0.0.1") || utils.localIP(ipaddr) )
    {
        /* pisensors will send this as properly formatted JSON, but as a string. */
        message = req.rawBody;
        log.info("Request rawBody from localhost: " + message);
    }
    else
    {
        try {
            message = crypto.decrypt("jarvis", req);
        }
        catch(ex){
            log.info("Request from unrecognized client: " + ex);
            res.send(401);
            return(next);
        }
    }

    /* Verify the request contains the "delete" command. */
    var json = JSON.parse(message);
    if ( json.command !== "delete" )
    {
        log.error("Unauthorized - request doesn't match delete.");
        res.send(401);
        return(next);
    }

    /* Verify the request contains the "uuid" key. */
    if ( typeof json.uuid === "undefined")
    {
        log.error("Unauthorized - request doesn't include remote sensor id.");
        res.send(400);
        return(next);
    }

    /* Find IPv4 of specified device */
    var ipaddr = utils.getIPv4(json.uuid);
    if ( ipaddr === null )
    {
        log.error("Delete request does not match any paired devices: " + json.uuid);
        res.send(400);
        return(next);
    }

    /* Delete the file */
    var stampDir = init.getIotDir();
    var filename = stampDir + ipaddr;
    fs.unlinkSync(filename);

    /* This responds before we know it worked, but that's fine for now. */
    res.json(200);
    return(next);
}
