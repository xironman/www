/*
 * IoT REST interface
 * The format for REST requests is
 *
 * CREATE Pair   POST /pair/<uuid>   From IoT Node              Pair an IoT node with the server
 * CREATE Device POST /device/<uuid> From IoT Node              Register a node
 * UPDATE Device PUT /device/<uuid>  From Jarvis, To IoT Node   Change device state with JSON
 * GET Device    GET /device/<uuid>  From Jarvis, To IoT Node   Get Device state, returned with JSON
 *
 * Version is added to header, as such:
 * Accept-Version: 1.0
 *
 * Additionally, JSON data may be included.  This would be data
 * specific to the cmd.
 */

/*
 * Import Classes
 */
const restify      = require('restify');
const fs           = require('fs');
const device       = require("./device");
const pair         = require("./pair");
const monitor      = require("./monitor");
const log          = require("./log");
const init         = require("./init");
const crypto       = require("./crypto");

/*
 * Initialization
 */
var help_file    = 'docs/help.txt';
var version_file = './version.txt';
var http_port    = 8165;
var debug        = false;

/*
 * Function that reads the version of this server from a file.
 */
function get_version() {
    var version_text = fs.readFileSync(version_file, 'utf8');
    version_text = version_text.replace(/(\r\n|\n|\r)/gm,"");
    return version_text;
}

/*
 * Command line argument processing
 * Options:
 * -v        (lowercase v) Set verbosity level
 * -V        (uppercase V) Show version number
 * -h        Show help text.
 * -T        Run in test mode.
 * -d        Data directory (for test mode).
 */
var validOptions = [
    'v', 'verbose', 'Verbose',
    'V', 'version', 'Version',
    'h', 'help', 'Help',
    'T', 'test', 'Test',
    'd', 'datadir', 'Datadir'
];
var args = require('minimist')(process.argv.slice(2), {
    boolean: [ 'h', 'V' ],
    alias: {
        h: 'help',
        v: 'verbose',
        V: 'Version',
        T: 'test',
        d: 'datadir'
    },
    default: {
        v: 0
    },
    stopEarly: true
});

if ( args.h )
{
    var help_text = fs.readFileSync(help_file, 'utf8');
    console.log(help_text);
    process.exit(1);
}
if ( args.V )
{
    var version_text = get_version();
    console.log("Version: " + version_text);
    process.exit(1);
}
if ( args.v > 0 )
{
    // Set verbosity level with log.set()
    log.set(args.v);
}
if ( args.d )
{
    init.setDataDir(args.d);
}
if ( args.T )
{
    debug = true;
}

/*
 * Do some initialization, after parsing options.
 */
init.setup(debug)

/*
 * Check for invalid arguments.
 */
function isOptionPresent(opt)
{
    for (var i = 0; i<validOptions.length; i++)
    {
        if (validOptions[i] == opt)
        {
            return true
        }
    }
    return false
}
for (var opt in args)
{
    if ( opt == "_" )
    {
        continue
    }
    if ( args[opt] )
    {
        if (!isOptionPresent(opt))
        {
            console.log("Unknown option: %s", opt)
            process.exit(1);
        }
    }
}

/*
 * Set http server options.
 */
var http_options = {
    version: '1.0.0'
};

/*
 * Instantiate our server.
 */
var http_server  = restify.createServer( http_options );

// Start our servers to listen on the appropriate ports
http_server.listen(http_port, function() {
    log.info(http_server.name + ' listening at ' + http_server.url);
});

/*
 * Check and set some headers for all requests/responses.
 */
http_server.pre(function (req, res, next) {
    res.setHeader('Accept-Version', get_version());
    if ( req.header("Accept-Version") != get_version() )
    {
        res.send(406, "Unsupported API version");
        return(next);
    }
    log.trace("Inbound request: " + req.method + ": " + req.url);
    log.trace(req.headers );
    return next();
});

/*
 * Map JSON body to parameters.
 */
http_server.use(restify.plugins.bodyParser({ mapParams: true }));

/*
 * HTTP Handlers
 * Since we encode messages to validate sender/receiver, all
 * messages that are not registration oriented must be POST.
 */
http_server.post ('/devices', device.list);
http_server.post ('/set/device', device.update);
http_server.del ('/set/device', device.delete);
http_server.get ('/query/devices', device.getDevices);
http_server.post ('/ping', device.ping);

http_server.post('/pair/iot/:uuid', pair.iot);
http_server.post('/pair/jarvis/:uuid', pair.jarvis);

http_server.get('/monitor*', monitor.get);

/* Default handler */
http_server.get ('/', function(req, res, next) {
        res.send(200);
        return(next);
    });

