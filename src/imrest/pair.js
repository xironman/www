/*
 * Pair Handler
 * 
 * Pairing can be requested by either IoT devices or Jarvis apps.
 * Pairing can only happen when the server is in pairing mode.
 */

/*
 * Import Classes
 */
var fs   = require('fs');
var util = require('util');
var log  = require('./log');
var init = require('./init');
var utils= require('./utils');

/*
 * Initialization
 */

/*
 * Private Methods
 */

/*
 * Add the key to the body of an IoT message.
 */
function add_key(body) {
    body["key"] = fs.readFileSync(key, 'utf8');
}

/*
 * Stuff a string into a json object.
 */
function add_key(body, key, value) {
    body[key] = value;
}

/*
 * Test that registration is enabled on this PiBox server.
 */
function registration_enabled() {
    var pairEnabled = init.getPairEnabled();

    /* The GPIO pin is reverse of what you'd think! */
    if ( pairEnabled == 0 ) {
        return false;
    }
    else {
        return true;
    }
}

/*
 * Check request path to see if "register" was included in the correct location.
 */
function is_registration(req) {
    var url = req.split( "/" );
    if ( url[3] == "register" )
        return 0;
    else
        return 1;
}

/*
 * UUID Handler
 * This stores the supplied UUID under /etc/ironman/<dest>/
 * to signifiy that the specific sending node is now registered.
 */
function handle_uuid(dest, req, res, next) {
    log.info("pair: handle_uuid has been called.");

    var ironmanDir = init.getIronmanDir();
    var stampDir = dest;

    log.info(req.params);
    var json = utils.stringify(req.params);

    if ( req.params.type )
    {
        log.info("Found type: " + req.params.type);
    }
    if ( req.params.description )
    {
        log.info("Found description: " + req.params.description);
    }

    /* Make sure destintation directory exists. */
    if ( !fs.existsSync(ironmanDir) )
    {
        fs.mkdirSync(ironmanDir, 0755);
    }
    if ( !fs.existsSync(stampDir) )
    {
        fs.mkdirSync(stampDir, 0755);
    }

    /*
     * Get the remote IP.  This should work because no
     * one outside of our subnet should be accessing us.
     * (Re: no Internet or mobile connections supported.)
     * Note: We strip the leading FFFF stuff if IPv6 was prefixed.
     */
    var requestIP = utils.toIPv4(req.connection.remoteAddress);

    /* Get name of file to write to. */
    var filename = stampDir + requestIP;

    /* We're registering, so delete any existing registration. */
    try { fs.unlinkSync(filename); }
    catch(ex) {}

    /*
     * Create a stamp file with the IP as the filename
     * and the request parameters as the content.
     */
    fs.writeFile(filename, json, function (err) {
        if (err) {
            log.error("Failed to write registration stamp file: " + filename + "; uuid= " + req.params.uuid);
            log.error("Reason: " + err);
            res.send(500);
            return(next);
        }
        log.info("Wrote registration stamp file: " + filename + "; uuid= " + req.params.uuid);

        /* Enable PAIR LED so user knows it worked.*/
        init.enablePairLED();
        setTimeout(init.disablePairLED, 3000);

        /* Notify caller that the registration succeeded. */
        res.send(200);
        return(next);
    });
}

/*
 * Public Methods
 */

/*
 * Register a new device.  This passes the request to the piboxd server, which will
 * ack the request separately.  Failures are silent, so devices that can't register
 * simply keep requesting.
 */
exports.iot = function (req, res, next) {
    log.info("pair.iot has been called.");

    /*
     * Registration is only allowed if the server is in registration mode.
     */
    if ( !registration_enabled() ) {
        init.disablePairLED();
        log.info("pair.iot registration is not enabled.");
        res.send(401);
        return(next);
    }

    /*
     * Handle an IoT device
     */
    var stampDir = init.getIotDir();
    handle_uuid(stampDir, req, res, next);
}

/*
 * Pair with a Jarvis node. 
 */
exports.jarvis = function (req, res, next) {
    log.info("pair.jarvis has been called.");

    /*
     * Registration is only allowed if the server is in registration mode.
     */
    if ( !registration_enabled() ) {
        init.disablePairLED();
        log.info("pair.jarvis registration is not enabled.");
        res.send(401);
        return(next);
    }

    /*
     * Handle a Jarvis node pairing.
     */
    var stampDir = init.getStampDir();
    handle_uuid(stampDir, req, res, next);
}
