/*
 * Cryptography
 * 
 * Encoding and decoding messages.
 * For an explanation as to why we use 128 instead of 256 (other than practical reasons
 * related to software requirements), see
 * http://security-architect.com/does-size-matter-aes-128-bit-encryption-is-probably-good-enough/
 */

/*
 * Import Classes
 */
const crypto = require('crypto');
const fs     = require('fs');
const log    = require('./log');
const init   = require('./init');
const utils  = require('./utils');

/*
 * Initialization
 */

/*
 * Private Methods
 */

function doDecode(iv, cryptkey, message)
{
    /* Decode IV */
    var ivbytes = Buffer.from(iv,'base64');

    /* decode message from base64 */
    var encryptedText = Buffer.from(message,'base64');

    // decrypt message
    var decipher = crypto.createDecipheriv('aes-128-cbc', cryptkey, ivbytes);
    var decrypted = decipher.update(encryptedText);
    decrypted += decipher.final();

    // return as string.
    return decrypted.toString();
}

/*
 * Public Methods
 * See: http://vancelucas.com/blog/stronger-encryption-and-decryption-in-node-js/
 * See: https://gist.github.com/vlucas/2bd40f62d20c1d49237a109d491974eb (builtin crypto)
 * See: https://github.com/chris-rock/node-crypto-examples/blob/master/crypto-ctr.js
 * See: https://www.npmjs.com/package/aes-js (aes-js module)
 * With base64:
 * See: https://gist.github.com/ericchen/3081970 (best?)
 * See: https://gist.github.com/AndiDittrich/4629e7db04819244e843
 * Also: Buffer.from("Hello World").toString('base64')
 * See: https://stackoverflow.com/questions/6182315/how-to-do-base64-encoding-in-node-js
 */

/*
 *  Front end to encrypting a message
 *  Returns string in JSON format
 *      { "iv":"string", "encrypted_message":"string" }
 */
exports.encrypt = function (cryptkey, message) {
    log.info("crypto.encrypt has been called.");
    log.info("message: " + message);

    /* Key must be 16 bytes */
    if ( cryptkey.length < 16 )
    {
        throw -1;
    }
    if ( cryptkey.length > 16 )
    {
        cryptkey = cryptkey.substring(0,16);
    }
    log.info("cryptkey: " + cryptkey);

    /* IV must be 16 bytes */
    let iv = crypto.randomBytes(16);

    /* Cipher will be padded with PKCS padding */
    let cipher = crypto.createCipheriv('aes-128-cbc', cryptkey, iv);
    log.info("Generated cipher");
    let encrypted = cipher.update(message, 'utf8', 'base64');
    log.info("Generated encrypted");
    encrypted += cipher.final('base64');
    log.info("encrypted to Buffer");

    var json = {};
    json.iv = Buffer.from(iv).toString('base64');
    json.message = encrypted;

    return json;
}

/*
 *  Front end to decrypting a message based on request data.
 *  src is either "jarvis" or "iot".
 *  Returns the decrypted string.
 */
exports.decrypt = function (src, req) {
    log.info("crypto.decrypt has been called for " + src);

    /* Get key for specific requester */
    var ipaddr = utils.toIPv4(req.connection.remoteAddress);
    var cryptkey = utils.getUUID(src, ipaddr);
    if ( cryptkey == null )
    {
        throw -1;
    }

    /* Key must be 16 bytes */
    if ( cryptkey.length < 16 )
    {
        throw -1;
    }
    if ( cryptkey.length > 16 )
    {
        cryptkey = cryptkey.substring(0,16);
    }
    log.info("cryptkey: " + cryptkey);

    /* Get IV and encoded/encrypted message from request */
    var iv = req.params.iv
    var message = req.params.message
    log.info("iv: " + iv);
    log.info("message: " + message);

    /* Decrypt and return it. */
    var response = doDecode(iv, cryptkey, message);
    return response;
}

/*
 *  Front end to decrypting a message based on uuid and a JSON response with iv and message.
 *  Returns the decrypted string.
 */
exports.decrypt_json = function (cryptkey, json) {
    log.info("crypto.decrypt_json has been called for " + cryptkey);

    /* Key must be 16 bytes */
    if ( cryptkey.length < 16 )
    {
        throw -1;
    }
    if ( cryptkey.length > 16 )
    {
        cryptkey = cryptkey.substring(0,16);
    }
    log.info("cryptkey: " + cryptkey);

    /* Get IV and encoded/encrypted message from request */
    var iv = json.iv
    var message = json.message
    log.info("iv: " + iv);
    log.info("message: " + message);

    /* Decrypt and return it. */
    var response = doDecode(iv, cryptkey, message);
    return response;
}

