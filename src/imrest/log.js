/*
 * Log Handler
 * 
 * Basic logging for miot server.
 */

/*
 * Import Classes
 */

/*
 * Initialization
 */
var log_level = 0;

/*
 * Private Methods
 */

/*
 * Output a logging message.
 */
function logger (header, msg) {
    if (typeof msg === 'string' )
    {
        console.log("%s: %s", header, msg);
    }
    else
    {
        console.log("%s: ", header, msg);
    }
}


/*
 * Public Methods
 */

/*
 * Set log level
 */
exports.set = function (new_level) {
    var level_name;
    switch(new_level) {
        case 0: return; 
                break;
        case 1: level_name = "ERROR";
                break;
        case 2: level_name = "WARN";
                break;
        case 3: level_name = "INFO";
                break;
        case 4: level_name = "TRACE";
                break;
        default: return;
                break;
    }
    log_level = new_level;
    console.log("Log level set to %s", level_name);
}

/*
 * Get log level
 */
exports.get = function () {
    return log_level;
}

/*
 * Log a trace message
 */
exports.trace = function (msg) {
    if ( log_level >= 4 )
    {
        logger("TRACE", msg);
    }
}

/*
 * Log an informational message
 */
exports.info = function (msg) {
    if ( log_level >= 3 )
    {
        logger("INFO", msg);
    }
}

/*
 * Log a warning message
 */
exports.warn = function (msg) {
    if ( log_level >= 2 )
    {
        logger("WARN", msg);
    }
}

/*
 * Log an error message
 */
exports.error = function (msg) {
    if ( log_level >= 1 )
    {
        logger("ERROR", msg);
    }
}

