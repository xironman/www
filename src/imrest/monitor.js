/*
 * Monitor Handler
 * 
 * Provides information about this monitor to requesting clients.
 */

/*
 * Import Classes
 */
var fs     = require('fs');
var log    = require('./log');
var init   = require('./init');
var crypto = require('./crypto');
var utils  = require('./utils');

/*
 * Initialization
 */

/*
 * Private Methods
 */

/*
 * Stuff a string into a json object.
 */
function add_key(body, key, value) {
    body[key] = value;
}

/*
 * Public Methods
 */

/*
 * Read monitor descriptor and return it.
 */
exports.get = function (req, res, next) {
    log.info("monitor.get has been called.");
    var descriptor = utils.getDescriptor();
    var ipaddr = utils.toIPv4(req.connection.remoteAddress);
    var device_uuid = utils.getUUID("jarvis", ipaddr);

    var json;
    try {
        crypt_json = crypto.encrypt(device_uuid, descriptor);
    }
    catch(err) {
        log.error(err);
        res.send(500, JSON.stringify(err));
        return(next);
    }
    log.info(JSON.stringify(crypt_json));

    res.send(200, crypt_json);
    return(next);
}
