/*
 * Common utility functions.
 */

/*
 * Import Classes
 */
var os     = require('os');
var fs     = require('fs');
var http   = require('http');
var log    = require('./log');
var init   = require('./init');
var crypto = require('./crypto');

/*
 * Initialization
 */

/*
 * Private Methods
 */
function loadUUID (source, requestIP) 
{
    var srcDir;
    if ( source.localeCompare("jarvis") == 0 ) 
    {
        srcDir = init.getStampDir();
    }
    else
    {
        srcDir = init.getIotDir();
    }
    var filename = srcDir + requestIP;
    if ( fs.existsSync(filename) )
    {
        var fileContent = fs.readFileSync(filename, 'utf8');
        var json = JSON.parse(fileContent);
        return json.uuid;
    }
    else
    {
        return null;
    }
}

/*
 * Public Methods
 */

/* Strip leading IPv6 prefix from an IPv4 address */
exports.toIPv4 = function (ipaddr) {
    return ipaddr.replace(/^.*:/, '');
}

/* Test if an IP address is one of ours */
exports.localIP= function (requestIP) {
    var ifaces = os.networkInterfaces();
    var found = 0;

    // Iterate all interfaces
    Object.keys(ifaces).forEach(function (ifname) {
        ifaces[ifname].forEach(function (iface) {

            // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
            if ('IPv4' !== iface.family || iface.internal !== false) {
                return;
            }

            // If this interface matches, note it as such.
            log.info("Have " + requestIP + ", want " + iface.address);
            if ( requestIP.localeCompare(iface.address) == 0 )
            {
                found = 1;
                return;
            }
        });
        if (found == 1)
        {
            return;
        }
    });
    if ( found == 1 ) 
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*
 * Get a device's UUID.
 */
exports.getUUID= function (source, requestIP) {
    return loadUUID( source, requestIP );
}

/*
 * Read the descriptor file into a text string.
 * If it doesn't exist, we return a default string.
 */
exports.getDescriptor = function () {
    var descriptorFile = init.getDescriptorFile();
    var text = "No descriptor available.";
    if ( fs.existsSync(descriptorFile) )
    {
        text = fs.readFileSync(descriptorFile, 'utf8');
        text = text.replace(/(\r\n|\n|\r)/gm,"");
    }
    return text;
}

/*
 * Convert a JSON object to a properly formatted JSON string.
 * Returns the newly constructed string.
 */
exports.stringify = function (obj) {
    var json="{";
    for(var key in obj) { 
        if (json.localeCompare("{") != 0 )
        {
            json += ",";
        }
        json += "\"" + key + "\":\"" + obj[key] + "\"";
    }
    json += "}";
    log.info("Converted JSON: " + json);
    return json;
}


/*
 * POST a request to a remote device.
 * Returns the body of the response as a string.
 * Caller is responsible for decrypting responses.
 */
exports.http_post = function (dest, api, post_data, callback) {
    // An object of options to indicate where to post to
    var length = post_data.length;
    log.info("Post data length: " + length);
    var post_options = {
        host: dest,
        port: '80',
        path: api,
        method: 'POST',
        headers: {
            'Content-Type': 'text/plain',
            'Content-Length': length
        }
    };

    // Set up the request and the reply.
    var post_req = http.request(post_options, function(res) {
        var response = "";
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            log.info("Got some data from sensor:" + chunk);
            response += chunk;
        });
        res.on('end',function(chunk){
            if (res.statusCode != 200) {
                log.info("API (" + api + ") call failed with response code " + res.statusCode);
                callback(dest, null);
            }
            else
            {
                callback(dest, response);
            }
        });
    }).on('error',function(error){
            // If this happens we may have a lost device - maybe we should drop it?
            log.error("API (" + api + ") call failed with error " + error);
        });

    // Post the request
    post_req.write(post_data);
    post_req.end();
}

/*
 * Search all IoT configurations for a matching uuid.  If found, return the IPv4 address, 
 * which is just the filename.  If not found return null.
 */
exports.getIPv4 = function (uuid) {
    /* Iterate all paired IoT devices. */
    var stampDir = init.getIotDir();
    var files = fs.readdirSync(stampDir);
    for(var i in files) 
    {
        /* Get the UUID for the device */
        var fileuuid = loadUUID("iot", files[i]);
        if ( uuid === fileuuid )
        {
            return(files[i]);
        }
    }
    return(null);
}
