#!/bin/bash
# Test the Ironman WWW server 
# -----------------------------------------------------------
# Initialization
# -----------------------------------------------------------

# Directories
IMREST="src/imrest"

# Files
LOGFILE="$(mktemp /tmp/imrest-log.XXXX)"

# Other
DOCLEANUP=1
CWD=$(pwd)
DATADIR="${CWD}/data"
VERBOSENAME="NONE"
VERBOSE=0
CURLV=""
SERVER_PID=0
TESTS=()
IDS=()
HTTP_PORT=8165
UUID="$(uuidgen)"
VERSION=$(cat ./version.txt)
LOCALHOST_IPV4="127.0.0.1"
LOCALHOST_IPV6="::1"
SERVER_ARGS=(
    -v3
    -T
    )

# How we find the modules to load.
RUNPATH="$(pwd)/$(dirname $0)"

# Ports and URLs
HTTP_URL="http://localhost:${HTTP_PORT}"

# Must be run from the top of the source tree.
if [[ ! -f util.mk ]]; then
    echo "You must run this from the top of the source tree."
    exit 1
fi

EMSG="[0m"
declare -A MSG=(
        [1]="[31m"
        [2]="[32m"
        [3]="[33m"
        [4]="[34m"
        [5]="[35m"
        [6]="[36m"
        [7]="[37m"
        [8]="[38m"
        [9]="[39m"
        [10]="[40m"
        [11]="[41m"
        [12]="[42m"
        [13]="[43m"
        [14]="[44m"
        [15]="[45m"
        [16]="[46m"
        [17]="[47m"
        [18]="[48m"
        [19]="[49m"
        )

#--------------------------------------------------------------
# Signal Traps
#--------------------------------------------------------------
# trap ctrl-c and call ctrl_c()
trap ctrl_c INT
function ctrl_c() {
    kill_server 0
    die 0
}
function die() {
    kill_server 0
    if [[ ${VERBOSE} -gt 0 ]]; then
        dump "${LOGFILE}" "Logfile"
    fi
    if [[ ${DOCLEANUP} -eq 1 ]]; then
        rm -f "${LOGFILE}"
    clear_data_dir
    fi
    exit ${1}
}

# -----------------------------------------------------------
# Functions
# -----------------------------------------------------------
# Show usage statement
function doHelp
{
    echo ""
    echo "$0 [-cs | -i ids | -t name | -v level]"
    echo "where"
    echo "-c          Don't do cleanup"
    echo "-s          Show color values for output"
    echo "-i ids      Comma separated list of subtests"
    echo "-t name     Name of test to enable, can be specified multiple times"
    echo "            Available tests:"
    echo "            Name           Description"
    echo "            header         Header tests"
    echo "                           Subtests: vers badvers"
    echo "            pair           Pair tests"
    echo "                           Subtests: device jarvis"
    echo "            monitor        Monitor tests"
    echo "                           Subtests: getid"
    echo "-v level    Enable verbose mode for debugging"
    echo "            Available levels:"
    echo "            ERROR     Shows only errors in debugging"
    echo "            WARN      Adds warning messages"
    echo "            INFO      Adds informatatory messages"
    echo ""
}

# Show available color options
function show_colors
{
    for color in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19; do
        msg ${color} "Color ID ${color}"
    done
    exit 0
}

# Generic message formatter
function msg
{
    echo -n ${MSG[$1]}
    echo -n "### $2"
    echo ${EMSG}
}

# Generic message formatter
function debug
{
    [[ ${VERBOSE} -eq 0 ]] && return

    local wantname=$1
    local want
    shift
    local msg="$@"

    # Convert names to numeric values
    case ${wantname} in
        'ERROR') want=1;;
        'WARN' ) want=2;;
        'INFO' ) want=3;;
        'TRACE') want=4;;
    esac

    if [[ ${want} -le ${VERBOSE} ]]; then
        local color
        case ${wantname} in
            'ERROR') color=11;; # white on red bg
            'WARN' ) color=15;; # white on purple bg
            'INFO' ) color=3;;  # yellow (or orange)
            'TRACE') color=6;;  # cyan 
        esac
        msg ${color} "${msg}"
    fi
}

function pass
{
    msg 2 "PASS: $1"
}

function fail
{
    msg 11 "FAIL: $1"
    die $2
}

function dump
{
    echo ""
    msg 3 "Dump of ${2}"
    cat "${1}"
    echo ""
}

# Start the NodeJS server
function start_server
{
    # Add the current level directory's "data" directory to server args
    SERVER_ARGS+=( "-d ${DATADIR}" )
    make_data_dir

    cd "${IMREST}"
    echo "Server args: ${SERVER_ARGS[@]}"
    echo "Logfile    : ${LOGFILE}"
    node miot.js ${SERVER_ARGS[@]} &>"${LOGFILE}" &
    SERVER_PID=$!
    sleep 2

    grep "restify listening" "${LOGFILE}" &>/dev/null
    if [[ $? -ne 0 ]]; then
        dump "${LOGFILE}" "Logfile"
        fail "${FUNCNAME[0]}: Server failed to start." 1
    else
        pass "${FUNCNAME[0]}: Server started."
    fi
}

# Kill the NodeJS server
# Arguments:
# $1    RC to use on exit; if set to -1 then don't exit.
function kill_server
{
    if [[ ${SERVER_PID} == 0 ]]; then
        fail "Server PID is not set.  Can't kill REST server."
        exit 1
    fi
    kill -15 ${SERVER_PID}
    if [[ $1 -gt 0 ]]; then
        exit $1
    fi
}

# Manage the data directory used for this testing.
function make_data_dir
{
    mkdir -p "${DATADIR}"
}
function clear_data_dir
{
    if [[ -d "${DATADIR}" ]]; then
        rm -rf "${DATADIR}"
    fi
}

# -----------------------------------------------------------
# Unit Tests
# -----------------------------------------------------------
source "${RUNPATH}/headers.sh"
source "${RUNPATH}/pair.sh"
source "${RUNPATH}/monitor.sh"

# -----------------------------------------------------------
# Command Line Processing
# -----------------------------------------------------------
while getopts ":csTi:t:v:" Option
do
    case $Option in
    c)  DOCLEANUP=0;;
    i)  IDS=( $(echo "${OPTARG}"| tr A-Z a-z | sed 's/,/ /g') );;
    s)  show_colors;;
    T)  SERVER_ARGS+=( -T );;
    t)  testname=`echo ${OPTARG} | tr A-Z a-z`
        TESTS+=( ${testname} )
        ;;
    v)  VERBOSENAME="${OPTARG}";;
    *)  doHelp; exit 0;;
    esac
done

# Error checking and setting up debug levels
case "${VERBOSENAME}" in 
    'NONE' ) VERBOSE=0;;
    'ERROR') VERBOSE=1;;
    'WARN' ) VERBOSE=2;;
    'INFO' ) VERBOSE=3;;
    'TRACE') VERBOSE=4; CURLV="-v";;
    *) msg 11 "Invalid verbose level"; doHelp; exit 0;;
esac

# We must specify at least one test category.
[[ "${TESTS[@]}" == "" ]] && { doHelp; exit 0; }

echo "TESTS: ${TESTS[@]}"
echo "IDS  : ${IDS[@]}"

# Make sure tests are valid.
for test in ${TESTS[@]}; do
    if [[ ! -n "$(type -t ${test})" ]] || [[ ! "$(type -t ${test})" == "function" ]]; then
        msg 11 "No such test: ${test}"
        exit 1
    fi
done

# -----------------------------------------------------------
# Main
# -----------------------------------------------------------

# Start the REST server
start_server

for test in ${TESTS[@]}; do
    ${test}
done

if [[ ${VERBOSE} -gt 0 ]]; then
    dump "${LOGFILE}" "Logfile"
fi
if [[ ${DOCLEANUP} -eq 1 ]]; then
    rm -f "${LOGFILE}"
    clear_data_dir
fi
kill_server 0
