#!/bin/bash
# Unit tests for headers.  
# Must be sourced by framework main to be used.
# -------------------------------------------------------------------------

# Test headers
header()
{
    for id in ${IDS[@]}; do
        header_${id}
    done
}

header_vers()
{
    > "${LOGFILE}"
    # Get headers from http server
    curl -is http://localhost:${HTTP_PORT} -X HEAD -H 'connection: close' &>"${LOGFILE}"

    # Check for Version header
    local version="$(grep "Accept-Version:" "${LOGFILE}"|cut -f2 -d" " | sed 's/\r//')"
    if [[ "${version:-}" != "${VERSION}" ]]; then
       dump "${LOGFILE}" "Logfile"
       fail "${FUNCNAME[0]}: Can't find Accept-Version in HTTP headers" 1
    fi
    pass "${FUNCNAME[0]}: Found Accept-Version in header."
}

header_badvers()
{
    > "${LOGFILE}"
    # Try to use bad header 
    curl -is http://localhost:${HTTP_PORT} -H 'Accept-Version: x.y' &>"${LOGFILE}"

    # Check Status Code
    local status_code="$(grep "^HTTP" "${LOGFILE}"|cut -f2 -d" " | sed 's/\r//')"
    if [[ "${status_code:-}" != "406" ]]; then
       dump "${LOGFILE}" "Logfile"
       fail "${FUNCNAME[0]}: Bad Accept-Version not caught by server" 1
    fi
    pass "${FUNCNAME[0]}: Bad Accept-Version caught by server."

    # Check for error message in body
    grep "Unsupported API version" "${LOGFILE}" &>/dev/null
    if [[ $? -ne 0 ]]; then
       dump "${LOGFILE}" "Logfile"
       fail "${FUNCNAME[0]}: Missing Accept-Version error message in body." 1
    fi
    pass "${FUNCNAME[0]}: Found Accept-Version error message in body."
}


