#!/bin/bash
# Unit tests for pairing.  
# Must be sourced by framework main to be used.
# -------------------------------------------------------------------------

# Test pairing between the server and either IoT devices or Jarvis.
pair()
{
    local testname="pair"
    local test_list=( ${IDS[@]} )

    if [[ "${test_list[@]}" == "" ]]; then
        test_list+=( jarvis )
        test_list+=( device )
    fi

    for id in ${test_list[@]}; do
        if [ ! -n "$(type -t ${testname}_${id})" ] || [ ! "$(type -t ${testname}_${id})" == function ]; then
            msg 11 "No such subtest: ${testname}_${id}"
            return
        fi
        ${testname}_${id}
    done
}

pair_device()
{
    # Make pair request.  
    curl -is http://localhost:${HTTP_PORT}/pair -X POST \
        -H "Accept-Version: 1.0" &>>"${LOGFILE}"

    # Check for key file in response
    # local version="$(grep "Accept-Version:" "${LOGFILE}"|cut -f2 -d" " | sed 's/\r//')"
    # if [[ "${version:-}" != "${VERSION}" ]]; then
       # dump "${LOGFILE}" "Logfile"
       # fail "${FUNCNAME[0]}: Can't find Accept-Version in HTTP headers" 1
    # fi
    pass "${FUNCNAME[0]}: Found Accept-Version in header."
}

# Test that pairing with a Jarvis app succeeds.
# All that's required is that the Jarvis app sends its UUID in the URI and that a file
# is created in the registration directory with a filename matching the UUID.
pair_jarvis()
{
    local results
    local status
    local reg_file

    # Create the stamp file to put us in registration mode
    touch ${DATADIR}/imnetconfig

    local registrationFile="${DATADIR}/ironman/jarvis/${UUID}"

    local URL="http://localhost:${HTTP_PORT}/pair/jarvis/${UUID}"
    debug INFO "Connecting to ${URL}"
    results="$(curl ${CURLV} -k -is ${URL} -X POST -H "Accept-Version: 1.0")"
    status="$(echo "${results}" | grep HTTP | cut -f2 -d" ")"

    # Check for 200 status response
    [[ ${status} -eq 200 ]] && pass "${FUNCNAME[0]}: Jarvis pair: REST status." || \
        fail "${FUNCNAME[0]}: wanted status = 200, got status = ${status}"

    # Check for registration file
    if [[ -f ${DATADIR}/ironman/jarvis/${LOCALHOST_IPV4} ]] || \
       [[ -f ${DATADIR}/ironman/jarvis/${LOCALHOST_IPV6} ]]; then
        pass "${FUNCNAME[0]}: Jarvis pair: registration file."
    else
        fail "${FUNCNAME[0]}: registration file missing: ${registrationFile}"
    fi

    # Validate registration file contents
    if [[ -f ${DATADIR}/ironman/jarvis/${LOCALHOST_IPV4} ]]; then
        reg_file=$(cat ${DATADIR}/ironman/jarvis/${LOCALHOST_IPV4})
    else 
        if [[ -f ${DATADIR}/ironman/jarvis/${LOCALHOST_IPV6} ]]; then
            reg_file=$(cat ${DATADIR}/ironman/jarvis/${LOCALHOST_IPV6})
        fi
    fi
    [[ "${reg_file}" == "${UUID}" ]] || \
        fail "${FUNCNAME[0]}: bad registration file content"

    # Clean up
    rm -f ${DATADIR}/imnetconfig
}


