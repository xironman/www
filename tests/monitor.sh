#!/bin/bash
# Unit tests for the monitor.  
# Must be sourced by framework main to be used.
# -------------------------------------------------------------------------

# Test monitor APIs
monitor()
{
    local testname="monitor"
    local test_list=( ${IDS[@]} )

    if [[ "${test_list[@]}" == "" ]]; then
        test_list+=( getid )
    fi

    for id in ${test_list[@]}; do
        if [ ! -n "$(type -t ${testname}_${id})" ] || [ ! "$(type -t ${testname}_${id})" == function ]; then
            msg 11 "No such subtest: ${testname}_${id}"
            return
        fi
        ${testname}_${id}
    done
}

# Retrieve the monitor id data.
monitor_getid()
{
    local results
    local status
    local decoded_message
    local defaultDescriptor="No descriptor available."

    # We only need the first 16 bytes of the UUID for use as the secret key.
    local key="${UUID:0:16}"
    debug TRACE "key length: ${#key}"

    # Then we need that key in hex for openssl.
    key="$(echo ${key} | xxd -l 16 -p)"

    # Make request to get monitor descriptor 
    local URL="http://localhost:${HTTP_PORT}/monitor"
    debug INFO "Connecting to ${URL}"
    results="$(curl ${CURLV} -k -is ${URL} -X GET -H "Accept-Version: 1.0")"
    status="$(echo "${results}" | grep HTTP | cut -f2 -d" ")"

    # Check for 200 status response
    [[ ${status} -eq 200 ]] && pass "${FUNCNAME[0]}: Monitor descriptor (default): REST status." || \
        fail "${FUNCNAME[0]}: Monitor descriptor (default): wanted status = 200, got status = ${status}"

    # Check JSON description  
    results="$(curl ${CURLV} -k -s ${URL} -X GET -H "Accept-Version: 1.0")"
    debug TRACE "results: ${results}"

    # Extract the message and IV fields.
    message="$( echo "${results}" | jq -r .message )"
    [[ "${message}" != "" ]] && pass "${FUNCNAME[0]}: Found message field in JSON." || \
        fail "${FUNCNAME[0]}: Missing message field in JSON."
    debug TRACE "message: ${message}"

    iv="$( echo "${results}" | jq -r .iv )"
    [[ "${iv}" != "" ]] && pass "${FUNCNAME[0]}: Found iv field in JSON." || \
        fail "${FUNCNAME[0]}: Missing iv field in JSON."
    debug TRACE "iv (JSON): ${iv}"

    # Decode and convert IV to hex for openssl
    iv="$(echo ${iv} | base64 -d | xxd -l 16 -p)"
    debug TRACE "iv (decoded): ${iv}"

    descriptor="$( echo "${message}" | openssl enc -base64 -d|openssl enc -d -aes-128-cbc -K ${key} -iv ${iv} )"
    debug TRACE "descriptor: ${descriptor}"

    [[ "${defaultDescriptor}" == "${descriptor}" ]] && \
        pass "${FUNCNAME[0]}: Monitor descriptor JSON (default)." || \
        fail "${FUNCNAME[0]}: Monitor descriptor JSON (default): wanted \"${defaultDescriptor}\", got \"${descriptor}\""

    # Create a descriptor file and verify we get it back
    local monitorDir="${DATADIR}/monitor"
    local descriptorFile="${monitorDir}/descriptor"
    local newDescriptor="Hello World"
    mkdir -p "${monitorDir}"
    echo "${newDescriptor}" >> "${descriptorFile}"

    results="$(curl ${CURLV} -k -s ${URL} -X GET -H "Accept-Version: 1.0")"
    debug TRACE "results: ${results}"

    # Extract the message and IV fields.
    message="$( echo "${results}" | jq -r .message )"
    [[ "${message}" != "" ]] && pass "${FUNCNAME[0]}: Found message field in JSON." || \
        fail "${FUNCNAME[0]}: Missing message field in JSON."
    debug TRACE "message: ${message}"

    iv="$( echo "${results}" | jq -r .iv )"
    [[ "${iv}" != "" ]] && pass "${FUNCNAME[0]}: Found iv field in JSON." || \
        fail "${FUNCNAME[0]}: Missing iv field in JSON."
    debug TRACE "iv (JSON): ${iv}"

    # Decode and convert IV to hex for openssl
    iv="$(echo ${iv} | base64 -d | xxd -l 16 -p)"
    debug TRACE "iv (decoded): ${iv}"

    descriptor="$( echo "${message}" | openssl enc -base64 -d|openssl enc -d -aes-128-cbc -K ${key} -iv ${iv} )"
    [[ "${newDescriptor}" == "${descriptor}" ]] && \
        pass "${FUNCNAME[0]}: Monitor descriptor JSON (generated)." || \
        fail "${FUNCNAME[0]}: Monitor descriptor JSON (generated): wanted \"${newDescriptor}\", got \"${descriptor}\""

    # cleanup
    rm -rf ${DATADIR}/monitor
}
