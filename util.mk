#----------------------------------------------------------------------
# Common variables and targets.
#
# Portions scarfed from mjhammels personal LFS build: mjhammel
#----------------------------------------------------------------------

#---------------------------------------------------------------------
# Commands used in this environment

# Wget command to retrieve source - put it all in the same directory.
WGET   := wget -P 
CRWGET := wget --passive-ftp -q -P 

#---------------------------------------------------------------------
# Tests for various things

#---------------------------------------------------------------------
# Headers for output messages
# Color messages - use like this:
# $(MSG2) some text $(EMSG)

# Reset colors to defaults.
export EMSG = "[0m"
export TM = $(shell date +'%F %r')

# Regular colors (add 10 for reverse video)
# 1. Red 2. Green 3. Yellow 4. Blue 5. Purple 6. Cyan 7. Gray
# 8. Underline 9. White 10. White 
export MSG1  = echo "\#\#\# $(MYTHBOXKNAME)[31m $(TM)"
export MSG2  = echo "\#\#\# $(MYTHBOXKNAME)[32m $(TM)"
export MSG3  = echo "\#\#\# $(MYTHBOXKNAME)[33m $(TM)"
export MSG4  = echo "\#\#\# $(MYTHBOXKNAME)[34m $(TM)"
export MSG5  = echo "\#\#\# $(MYTHBOXKNAME)[35m $(TM)"
export MSG6  = echo "\#\#\# $(MYTHBOXKNAME)[36m $(TM)"
export MSG7  = echo "\#\#\# $(MYTHBOXKNAME)[37m $(TM)"
export MSG8  = echo "\#\#\# $(MYTHBOXKNAME)[38m $(TM)"
export MSG9  = echo "\#\#\# $(MYTHBOXKNAME)[39m $(TM)"
export MSG10 = echo "\#\#\# $(MYTHBOXKNAME)[40m $(TM)"
export MSG11 = echo "\#\#\# $(MYTHBOXKNAME)[41m $(TM)"
export MSG12 = echo "\#\#\# $(MYTHBOXKNAME)[42m $(TM)"
export MSG13 = echo "\#\#\# $(MYTHBOXKNAME)[43m $(TM)"
export MSG14 = echo "\#\#\# $(MYTHBOXKNAME)[44m $(TM)"
export MSG15 = echo "\#\#\# $(MYTHBOXKNAME)[45m $(TM)"
export MSG16 = echo "\#\#\# $(MYTHBOXKNAME)[46m $(TM)"
export MSG17 = echo "\#\#\# $(MYTHBOXKNAME)[47m $(TM)"
export MSG18 = echo "\#\#\# $(MYTHBOXKNAME)[48m $(TM)"
export MSG19 = echo "\#\#\# $(MYTHBOXKNAME)[49m $(TM)"

# Default color
export MSG = echo "\#\#\# "

showMsg:
	@$(MSG) "MSG Formats" $(EMSG); 
	@$(MSG1) "MSG1" $(EMSG); 
	@$(MSG2) "MSG2" $(EMSG); 
	@$(MSG3) "MSG3" $(EMSG); 
	@$(MSG4) "MSG4" $(EMSG); 
	@$(MSG5) "MSG5" $(EMSG); 
	@$(MSG6) "MSG6" $(EMSG); 
	@$(MSG7) "MSG7" $(EMSG); 
	@$(MSG8) "MSG8" $(EMSG); 
	@$(MSG9) "MSG9" $(EMSG); 
	@$(MSG10) "MSG10" $(EMSG); 
	@$(MSG11) "MSG11" $(EMSG); 
	@$(MSG12) "MSG12" $(EMSG); 
	@$(MSG13) "MSG13" $(EMSG); 
	@$(MSG14) "MSG14" $(EMSG); 
	@$(MSG15) "MSG15" $(EMSG); 
	@$(MSG16) "MSG16" $(EMSG); 
	@$(MSG17) "MSG17" $(EMSG); 
	@$(MSG18) "MSG18" $(EMSG); 
	@$(MSG19) "MSG19" $(EMSG); 


#---------------------------------------------------------------------
# GENERIC TARGETS

# Print a usage message
usage help:
	@if [ "$$PAGER" ]; then $$PAGER docs/build.help; \
  	else more docs/build.help; \
	fi

# ---------------------------------------------------------------
# Check if the user is running as root.  Some build targets
# require this.
# ---------------------------------------------------------------
checkSudo: 
	@if [ `id -u` -ne 0 ] ; then \
		$(MSG1) "You must run the build as the root user." $(EMSG); \
		exit 1; \
	fi

# Fetching software.  
# Set S to the name of the package.
# Set U to the URL to retrieve S from.
# Set D to the download directory.
# Set C to either z (gzip) or j (bzip2) for unpacking.
getsw-only:
	@if [ ! -f $(D)/$(S) ]; then \
		$(MSG7) "Retrieving $(S)." $(EMSG); $(WGET) $(D) $(U)/$(S); fi
	@if [ ! -f $(D)/$(S) ]; then \
		$(MSG1) "Failed to retrieve $(S)." $(EMSG); exit 1; fi

getsw: getsw-only
	@N=$(shell echo "$(S)"|sed -e s/\.tar\..*//g) && \
	if [ ! -d $(D)/$$N ]; then \
		$(MSG7) "Unpacking $(S)..." $(EMSG); \
		cd $(D) && tar x$(C)f $(S); \
	fi

getpatch:
	@if [ ! -f $(D)/$(S) ]; then $(WGET) $(D) $(U)/$(S); fi
	@if [ ! -f $(D)/$(S) ]; then \
		$(MSG1) "Failed to retrieve $(S)." $(EMSG); exit 1; fi


