## Synopsis

Ironman WWW is a package providing two web services for Ironman.  The first is a PHP-based network configuration service.  This utilizes the PiBox Network Config protocols and piboxd for submitting network changes.

The second service is a NodeJS-based REST API for use with IoT device management under Ironman.

## Build

Ironman WWW has a GNU Make build system whose primary purpose is to package the web services.

The build depends on some environment variables being set.  Copy the file docs/ironman.sh to another directory and edit it as appropriate.  After editing, source the script:

    . <path_to_script>/ironman.sh

Then setup your environment by running the function in that script:

    ironman www

Now you're ready to retrieve the source.  Run the following command to see how to clone the source tree:

    cd?

This will tell you exactly what to do.  After you clone the source you can do a complete build just by typing:

    sudo make pkg

or 

    sudo make pkg HW=piplayer

Available build environment variables:

    HW=media, piplayer, pisentry, pistore (sets the banner for imwww)

HW=<> disables installation of the init script so it can be enabled/disabled from the pinet application.  This lets the app be used to configure a node without using the web-clone of pibox-network-config or pibox-network-config GUI.  It also disables installation of the imrest server, which is only used by Ironman sensor nodes.

To clean up:

    sudo make clobber

To get additional help on available targets, use the following command:

    make help

This will display all the available targets and environment variables with explanations on how to use them.

## Installation

Ironman WWW is packaged in an opkg format.  After building, look in the opkg directory for an .opk file.  This is the file to be installed on the target system.

To install the package on the target, copy the file to the system or the SD card that is used to boo the system.  The use the following command.

    opkg install <path to file>/imwww_1.0_arm.opk

If the package has been installed previously, use the following command to reinstall it.

    opkg --force-reinstall install <path to file>/imwww_1.0_arm.opk

## Contributors

For more information on building, see the wiki:
http://www.graphics-muse.org/wiki/pmwiki.php

## License

0BSD

