#!/usr/bin/env bash
# Run a web server, either PHP or NodeJS
# ------------------------------------------------

# ---------------------------------------------
# Initialization
# ---------------------------------------------

# DOCROOT and DATADIR are just for the PHP server
DOCROOT="/home/httpd/imwww"
DATADIR=${DOCROOT}/data
NOAPF="noap"
NOAPD="/etc"

# PIDFILE holds the process id of the server in production mode.
PIDFILE="/var/run/webserver.pid"

# NOAP disables the AP configuration component.
NOAP=0

# KEEP is used only for testing.
KEEP=0

# PROD is the default, it means Production (not testing).
PROD=0

# KILL is used to kill any server running in Production.
KILL=0

# NODEJS determines whether we start the PHP or NODEJS server.
NODEJS=0

# NODEJS_DIR is where the nodejs web service is installed.
NODEJS_DIR=/home/httpd/imrest

# How do we start daemons; we'll set this after parsing CLI arguments.
START_DAEMON=""

# How do we stop daemons; we'll set this after parsing CLI arguments.
STOP_DAEMON=""

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

# ---------------------------------------------
# functions
# ---------------------------------------------

# Handle CTRL-c (exit from server)
function ctrl_c() 
{
    if [[ ${KEEP} -eq 0 ]]; then
        if [[ -e ${DATADIR} ]]; then
            echo "Cleaning up ${DATADIR}"
            rm -rf "${DATADIR}"
        fi
    fi
}

# Show usage message
function doHelp
{
    echo ""
    echo "$0 [-aKknp | -r docroot]"
    echo "where"
    echo "-a            Disable AP component"
    echo "-n            Run the NodeJS server"
    echo "-K            Kill whichever server is running"
    echo "-p            Run in production mode"
    echo "              Default: Test Mode, which does not save server PID."
    echo "-k            Keep debug data after running"
    echo "-r docroot    Set the docroot"
    echo "              Default: ${DOCROOT}"
    echo "-P pidfile    Set the pidfile location (fully qualified path)"
    echo "              Default: ${PIDFILE}"
    echo ""
    echo "Start a PHP web server using the source tree as a document root."
    echo "Automatically starts in debug mode."
    echo ""
}

die()
{
    echo "$1"
    exit 1
}

# ---------------------------------------------
# Process command line options
# ---------------------------------------------
while getopts ":aKknpP:r:" Option
do
    case $Option in
    a) NOAP=1;;
    p) PROD=1;;
    n) NODEJS=1;;
    P) PIDFILE=${OPTARG};;
    r) DOCROOT=${OPTARG};;
    k) KEEP=1;;
    K) KILL=1;;
    *) doHelp; exit 0;;
    esac
done

# Now we can build the start/stop daemon commands.
START_DAEMON="start-stop-daemon -S -q -m --pidfile ${PIDFILE} -b --oknodo -x"
STOP_DAEMON="start-stop-daemon -K --pidfile ${PIDFILE} --retry 5"

# ---------------------------------------------
# Main
# ---------------------------------------------

# If requested, try to kill a running server, if any.
if [[ ${KILL} -eq 1 ]]; then

    rm -f "${NOAPD}/${NOAPF}"

    if [[ -e "${PIDFILE}" ]]; then
        echo "Stopping web server."
        ${STOP_DAEMON}
        if [[ $? -eq 0 ]]; then
            rm -f "${PIDFILE}"
            exit 0
        else
            echo "Failed to stop daemon.  Forcing removal of PID file."
            rm -f "${PIDFILE}"
            exit 1
        fi
    else
        echo "Web server daemon is not running."
        exit 0
    fi
fi

# Setup for use in debug environment for imwww
if [[ ${PROD} -eq 0 ]] && [[ ${NODEJS} -eq 0 ]]; then
    DOCROOT="$(pwd)/src/imwww"
    DATADIR="${DOCROOT}/data"
    NOAPD="${DATADIR}"
    echo "DOCDIR : ${DOCDIR}"
    echo "DATADIR: ${DATADIR}"
    echo "NOAPD  : ${NOAPD}"
    echo "PIDFILE: ${PIDFILE}"

    # Cleanup (if necessary) and create the data directory
    rm -f "${PIDFILE}"
    rm -rf "${DATADIR}"
    mkdir -p "${DATADIR}"

    # Add the stamp file for debugging
    touch "${DATADIR}/debug"
    touch "${DATADIR}/pair"

    ls -al "${DATADIR}"
fi

# Setup for use in debug environment for imrest
if [[ ${PROD} -eq 0 ]] && [[ ${NODEJS} -eq 1 ]]; then
    NODEJS_DIR=src/imrest
    echo "NODEJS_DIR : ${NODEJS_DIR}"
fi

# If requested, disable the AP component of imwww.
if [[ ${NODEJS} -eq 0 ]]  && [[ ${NOAP} -eq 1 ]]; then
    touch "${NOAPD}/${NOAPF}"
fi

# Run the server.
if [[ ${NODEJS} -eq 1 ]]; then
    # run nodejs server
    cd "${NODEJS_DIR}" || die "Can't change to ${NODEJS_DIR}"
    if [[ ${PROD} -eq 0 ]]; then
        # Debug mode - for testing during development
        node ./miot.js -v3 -T
    else
        # Production mode
        ${START_DAEMON} /usr/bin/node -- ./miot.js
    fi
else
    if [[ ${PROD} -eq 0 ]]; then
        # Debug mode - for testing during development
        php -S 0.0.0.0:1337 -t "${DOCROOT}"
        rm -f "${NOAPD}/${NOAPF}"
    else
        # Production mode
        ${START_DAEMON} /usr/bin/php -- -S 0.0.0.0:1337 -t "${DOCROOT}"
    fi
fi
